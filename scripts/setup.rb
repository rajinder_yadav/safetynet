# Author: Rajinder Yadav <safetynet@devmentor.org>
# Date: Jan 15, 2012
# Web : safetynet.devmentor.org

def createMakefiles( platform )   
   # linux will use gnu make
   if( platform.fLinuxOS )
      createGNUMakefile( platform )
   elsif( platform.fWinOS )
      # windows may have nmake or mingw compiler
      if( platform.fMinGW )
         createMinGWMakefiles( platform )
      end
      if( platform.fVisualCPP )
         createNMakefiles( platform )
      end
   end
end


def createCodeBlocksProject( platform )

   return if( !$fGenerateCodeBlocks )

   Dir.chdir(CODEBLOCKS_FOLDER)

   if( platform.fLinuxOS )

      generateFlags = "#{$sGenerateFlags} -D UT_LINUX_BUILD=1"
      generateFlags += " -D UT_QT5SDK_BUILD=1" if( $fGenerateWithQt5 && platform.fQT5SDK )
      puts "==> Generating CodeBlocks + GNU Linux make project"
      puts "==> CMake Flags: #{generateFlags}"
      notifyCMakeProject system( %Q!cmake -G "CodeBlocks - Unix Makefiles" #{generateFlags} ../src! )

   elsif( platform.fWinOS )
      # on Windows, when both Visual Studio and CodeBlocks in stalled
      # we default to generating codeblocks + mingw
      if( platform.fMinGW )

         generateFlags = "#{$sGenerateFlags} -D UT_MINGW_BUILD=1"
         generateFlags += " -D UT_QT5SDK_BUILD=1" if( $fGenerateWithQt5 && platform.fQT5SDK )
         puts "==> Generating Windows CodeBlocks + MinGW project"
         puts "==> CMake Flags: #{generateFlags}"
         notifyCMakeProject system( %Q!cmake -G "CodeBlocks - MinGW Makefiles" #{generateFlags} ../src! )

      elsif( platform.fVisualCPP )

         generateFlags = "#{$sGenerateFlags} -D UT_NMAKE_BUILD=1"
         puts "==> Generating Windows CodeBlocks + NMake project"
         puts "==> CMake Flags: #{generateFlags}"
         notifyCMakeProject system( %Q!cmake -G "CodeBlocks - NMake Makefiles" #{generateFlags} ../src! )

      end
   end
   Dir.chdir("..")
end

def createEclipseProject( platform )
	return if( !$fGenerateEclipse )
	Dir.chdir(ECLIPSE_FOLDER)
   if( platform.fLinuxOS )

      generateFlags = "#{$sGenerateFlags} -D UT_LINUX_BUILD=1"
      generateFlags += " -D UT_QT5SDK_BUILD=1" if( $fGenerateWithQt5 && platform.fQT5SDK )
      puts "==> Generating CodeBlocks + GNU Linux make project"
      puts "==> CMake Flags: #{generateFlags}"
      notifyCMakeProject system( %Q!cmake -G "Eclipse CDT4 - Unix Makefiles" #{generateFlags} ../src! )

   elsif( platform.fWinOS )
      # on Windows, when both Visual Studio and CodeBlocks in stalled
      # we default to generating codeblocks + mingw
      if( platform.fMinGW )

#         generateFlags = "#{$sGenerateFlags} -D UT_MINGW_BUILD=1"
#         generateFlags += " -D UT_QT5SDK_BUILD=1" if( $fGenerateWithQt5 && platform.fQT5SDK )
#         puts "==> Generating Windows CodeBlocks + MinGW project"
#         puts "==> CMake Flags: #{generateFlags}"
#         notifyCMakeProject system( %Q!cmake -G "CodeBlocks - MinGW Makefiles" #{generateFlags} ../src! )

      elsif( platform.fVisualCPP )

#         generateFlags = "#{$sGenerateFlags} -D UT_NMAKE_BUILD=1"
#         puts "==> Generating Windows CodeBlocks + NMake project"
#         puts "==> CMake Flags: #{generateFlags}"
#         notifyCMakeProject system( %Q!cmake -G "CodeBlocks - NMake Makefiles" #{generateFlags} ../src! )

      end
   end

	Dir.chdir("..")
end

#===========================
# general utility functions
#===========================
def showUsage
   puts "./gen <source_folder> [build | clean] [options]\n\n"
   puts "  build          - builds binaries, does not perfom clean (faster)"
   puts "  clean          - deleted build folder and re-builds binaries\n\n"
   puts "Options - at least one is required"
   puts "  g:make         - use on Linux to generate a GNU CMake Makefile"
   puts "  g:nmake        - use on Windows to generate a Visual C++ CMake Makefile"
   puts "  g:mingw        - use on Windows to generate a MinGW CMake Makefile"
   puts "  g:qt           - use to add Qt5 Network CMake build support\n\n"
   puts "Optional"
   puts "  g:visualstudio - generate a VisualStudio project folder"
   puts "  g:codeblocks   - generate CodeBlocks project folder"
   puts "  g:eclipse      - generate Eclipse Makefile project"
   puts "  g:kdevelop     - generate KDevelop3 Makefile project"
end
