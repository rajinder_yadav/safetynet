# Author: Rajinder Yadav <safetynet@devmentor.org>
# Date: Jan 15, 2012
# Web : safetynet.devmentor.org

def prepareLinuxBuildFolder( platform )
	# check script argument
	case(ARGV[1])
	when "clean"
		puts "==> Deleting all projects build folders"
      FileUtils.rm_rf(BUILD_FOLDER)			if( Dir.exist?(BUILD_FOLDER) )
      FileUtils.rm_rf(CODEBLOCKS_FOLDER)  if( Dir.exist?(CODEBLOCKS_FOLDER) ) 
      FileUtils.rm_rf(KDEVELOP_FOLDER)		if( Dir.exist?(KDEVELOP_FOLDER) )
      FileUtils.rm_rf(ECLIPSE_FOLDER)		if( Dir.exist?(ECLIPSE_FOLDER) )
	when "build"
		puts "==> Deleting build folder"
		FileUtils.rm_rf(BUILD_FOLDER)
	when "?"
		showUsage
		exit ERROR_USAGE
	end

	if( !Dir.exist?(BUILD_FOLDER) )
		puts "==> Creating build folder"
		Dir.mkdir(BUILD_FOLDER)
	end

	if( $fGenerateCodeBlocks && platform.fCodeblocks && !Dir.exist?(CODEBLOCKS_FOLDER) )
		puts "==> Creating CodeBlocks project folder"
		Dir.mkdir(CODEBLOCKS_FOLDER)
	end

   if( $fGenerateKDevelop3 && !Dir.exist?(KDEVELOP_FOLDER) )
      puts "==> Creating KDelevop3 Makefile build folder"
      Dir.mkdir(KDEVELOP_FOLDER)
   end
   
   if( $fGenerateEclipse && !Dir.exist?(ECLIPSE_FOLDER) )
      puts "==> Creating Eclipse Makefile build folder"
      Dir.mkdir(ECLIPSE_FOLDER)
   end

end

def createGNUMakefile( platform )
	return if( !$fGenerateMake )
   generateFlags = "#{$sGenerateFlags} -D UT_LINUX_BUILD=1"
   generateFlags += " -D UT_QT5SDK_BUILD=1" if( $fGenerateWithQt5 && platform.fQT5SDK )
	Dir.chdir(BUILD_FOLDER)
   puts "==> Generating GNU Linux Makefiles"
   puts "==> CMake Flags: #{generateFlags}"
   notifyCMake system( %Q!cmake -G "Unix Makefiles" #{generateFlags} ../src! )
   Dir.chdir("..")
end

def buildLinuxBinaries( platform )
	return if( !$fGenerateMake )
	Dir.chdir(BUILD_FOLDER)
	puts "==> Running GNU Linux make"
	notifBuild system 'make'
	Dir.chdir("..")
end
