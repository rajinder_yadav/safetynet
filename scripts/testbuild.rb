#!/usr/local/bin/ruby
#
# Author: Rajinder Yadav <safetynet@devmentor.org>
# Date: Feb 7, 2012
# Web : safetynet.devmentor.org

def runUnitTest( platform )

   # find the test executable name from CMakeLists.txt   
   if( Dir.exist?( "build_make/test/" ) )
      # we are building the parent project, look in test sub-folder
      cmakeFile = IO.readlines( 'test/CMakeLists.txt' )
   else
      # we are buildinf the test folder
      cmakeFile = IO.readlines( 'CMakeLists.txt' )
   end

   line = cmakeFile.grep( /add_executable/ ).to_s
   i = line.index( '(' )
   testBinaryName = line[(i+1)..-1].split[0]

   if( platform.fWinOS )
      if( Dir.exist?( "build_nmake/test/" ) || Dir.exist?( "build_mingw/test/" ) )
         if( platform.fQT5SDK )
            # we don't know if UnitTest DLL was built using Qt5SDK suuport or not
            # so copy Qt DLLs into both build folders

            # copy Qt5 DLLs into mingw build folder
            if( $fGenerateMinGW )
               FileUtils.cp( ENV["QT_ROOT"]+"/lib/QtGui4.dll", "build_mingw/test/" )
               FileUtils.cp( ENV["QT_ROOT"]+"/lib/QtCore4.dll", "build_mingw/test/" )
               FileUtils.cp( ENV["QT_ROOT"]+"/lib/QtNetwork4.dll", "build_mingw/test/" )
            end

            # copy Qt5 DLLs into nmake build folder
            if( $fGenerateNMame )
               FileUtils.cp( ENV["QT_ROOT"]+"/lib/QtGui4.dll", "build_nmake/test/" )
               FileUtils.cp( ENV["QT_ROOT"]+"/lib/QtCore4.dll", "build_nmake/test/" )
               FileUtils.cp( ENV["QT_ROOT"]+"/lib/QtNetwork4.dll", "build_nmake/test/" )
            end
         end

         # copy UnitTest DLL
         FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/UnitTest/build_nmake/UnitTest.dll", "build_nmake/test/" ) if( $fGenerateNMame )
         FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/UnitTest/build_mingw/libUnitTest.dll", "build_mingw/test/" ) if( $fGenerateMinGW )

         system( "./build_nmake/test/#{testBinaryName}" ) if( $fGenerateNMame )
         system( "./build_mingw/test/#{testBinaryName}" ) if( $fGenerateMinGW )
      else
         if( platform.fQT5SDK )
            # we don't know if UnitTest DLL was built using Qt5SDK suuport or not
            # so copy Qt DLLs into both build folders

            # copy Qt5 DLLs into mingw build folder
            if( $fGenerateMinGW )
               FileUtils.cp( ENV["QT_ROOT"]+"/lib/QtGui4.dll", "build_mingw/" )
               FileUtils.cp( ENV["QT_ROOT"]+"/lib/QtCore4.dll", "build_mingw/" )
               FileUtils.cp( ENV["QT_ROOT"]+"/lib/QtNetwork4.dll", "build_mingw/" )
            end

            # copy Qt5 DLLs into nmake build folder
            if( $fGenerateNMame )
               FileUtils.cp( ENV["QT_ROOT"]+"/lib/QtGui4.dll", "build_nmake/" )
               FileUtils.cp( ENV["QT_ROOT"]+"/lib/QtCore4.dll", "build_nmake/" )
               FileUtils.cp( ENV["QT_ROOT"]+"/lib/QtNetwork4.dll", "build_nmake/" )
            end
         end

         # copy UnitTest DLL
         FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/UnitTest/build_nmake/UnitTest.dll", "build_nmake/" ) if( $fGenerateNMame )
         FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/UnitTest/build_mingw/libUnitTest.dll", "build_mingw/" ) if( $fGenerateMinGW )

         system( "./build_nmake/#{testBinaryName}" ) if( $fGenerateNMame )
         system( "./build_mingw/#{testBinaryName}" ) if( $fGenerateMinGW )
      end

   elsif( platform.fLinuxOS )
      if( Dir.exist?( "build_make/test/" ) )
         system( "./build_make/test/#{testBinaryName}" )
      else
         system( "./build_make/#{testBinaryName}" )
      end
   end

end
