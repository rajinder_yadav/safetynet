#!/usr/bin/env ruby
#
# Author: Rajinder Yadav <safetynet@devmentor.org>
# Date: Dec 18, 2011
# Web : safetynet.devmentor.org

# NOTE : If build with MinGW inplace of NMake, you may need to
# hardcode path as MinGW is not happy with forward slash in path

require "fileutils"
require "shell"
require_relative "platform"
require_relative "setup"
require_relative "linux_setup"
require_relative "windows_setup"
require_relative "notifier"
require_relative "testbuild"

#require_relative "command_parser"

#=================
# Global constants
#=================
ERROR_SUCCESS        = 0
ERROR_USAGE          = 1
ERROR_BUILD_FAILED   = 10
ERROR_CMAKE_MAKEFILE = 11
ERROR_CMAKE_PROJECT  = 12
ERROR_ENV_VAR        = 13

$errCode = ERROR_SUCCESS

# CMake buildtype
BUILD_TYPE       = "Release"

# These flags control what CMake Makefile will get generated
$fGenerateMake       = false
$fGenerateMinGW      = false
$fGenerateNMame      = false
$fGenerateVCPP       = false
$fGenerateWithQt5    = false
$fGenerateCodeBlocks = false
$fRunUnitTest        = false
$sGenerateFlags      = "-D CMAKE_BUILD_TYPE=#{BUILD_TYPE}"

$fGenerateKDevelop3 = false
$fGenerateEclipse   = false

platform = Platform.new

# NOTE: NMake with Qt5SDK not supported
for param in ARGV
   case( param )
   when "g:nmake"
      # CMake define UT_NMAKE_BUILD (Windows)
      #$sGenerateFlags += " -D UT_NMAKE_BUILD=1"
      $fGenerateNMame = true
   when "g:mingw"
      # CMake define UT_MINGW_BUILD (Windows)
      #$sGenerateFlags += " -D UT_MINGW_BUILD=1"
      $fGenerateMinGW = true
   when "g:make"
      # CMake define UT_LINUX_BUILD (Linux, ...)
      #$sGenerateFlags += " -D UT_LINUX_BUILD=1"
      $fGenerateMake = true
   when "g:qt"
      # CMake define UT_QT5SDK_BUILD (Windows, Linux, ...)
      #$sGenerateFlags += " -D UT_QT5SDK_BUILD=1"
      $fGenerateWithQt5 = true
   when "g:visualstudio"
      # Visual Studio (Windows)
      $fGenerateVCPP = true
   when "g:codeblocks"
      # CodeBlocks (Windows, Linux, ...)
      $fGenerateCodeBlocks = true
   when "r:test"
      $fRunUnitTest = true
   when "doc"
      if( platform.fLinuxOS )
         system( "xdg-open http://safetynet.devmentor.org/safetynet_guide.html &" ) 
      else
         system( "start http://safetynet.devmentor.org/safetynet_guide.html" )
      end
      exit true
   when "help"
      if( platform.fLinuxOS )
         system( "xdg-open http://safetynet.devmentor.org/documents/gen.html &" ) 
      else
         system( "start http://safetynet.devmentor.org/documents/gen.html" )
      end
      exit true
   when "g:kdevelop3"
      $fGenerateKDevelop3 = true
   when "g:eclipse"
      $fGenerateEclipse = true
   end
end

BUILD_FOLDER      = platform.fVisualCPP ? "build_nmake" : "build_make"
CODEBLOCKS_FOLDER = "build_codeblocks"
VCPP_FOLDER       = "build_vs"
MINGW_FOLDER      = "build_mingw"
KDEVELOP_FOLDER   = "build_kdevelop"
ECLIPSE_FOLDER    = "build_eclipse"

begin
if( platform.fLinuxOS )
   $fColorizeText  = true
elsif( Gem::Specification::find_by_name 'win32console' )
   require "win32console" if( platform.fWinOS )
   $fColorizeText = true
end
rescue Gem::LoadError => e
   $fColorizeText = false
end

puts "==> Desktop: #{platform.desktop}"

# we are changing into the project (root) source folder
# this needs to be the first argument passed to the build script
if( Dir.exist?( ARGV[0] ) )
	Dir.chdir( ARGV[0] )
else
	puts "==> Error! Source folder does not exist.\n\nUsage:\n"
	showUsage
	exit false
end

# clean and create build folders
if( platform.fLinuxOS )
	prepareLinuxBuildFolder( platform )
elsif( platform.fWinOS )
	prepareWinBuildFolder( platform )
end

# generate make files and project files
createMakefiles( platform )

# generate visualstudio project files
createVisualCPPProject( platform ) if( platform.fVisualCPP )

# on the windows platform build codeblocks + mingw if it exist
# then try to build codeblocks + nmake
createCodeBlocksProject( platform ) if( $fGenerateCodeBlocks )
createKDevelopProject( platform ) if( $fGenerateKDevelop3 )
createEclipseProject( platform ) if( $fGenerateEclipse )

# unless we only want to generate a project, also build the binaries
# the binaries will be build if any of the following switches
# passed, g:make, m:nmake or g:mingw is
if( ARGV[1] != "project" )
   if( platform.fLinuxOS )
   	buildLinuxBinaries( platform )
   elsif( platform.fWinOS )
   	buildWinBinaries( platform )
   end
end

runUnitTest( platform ) if( $fRunUnitTest )

Dir.chdir( ".." )
notifyExit( $errCode, platform.desktop )
