# Author: Rajinder Yadav <safetynet@devmentor.org>
# Date: Jan 15, 2012
# Web : safetynet.devmentor.org

class ParamParser

   attr_accessor :fBuild
   attr_accessor :fClean
   attr_accessor :fTest
   attr_accessor :fSetup

   attr_accessor :fRelease
   attr_accessor :fDebug

   def initialize
      @fBuild = false
      @fClean = false
      @fTest  = false

      @param_list = ARGV
      @source_folder = @param_list.shift

      while( !@param_list.empty? )
         token = @param_list.shift

         case( token )
         when "build"
            @fBuild = true
         when "clean"
            @fClean = true
         when "test"
            @fTest = true
         when "setup"
            # fetch code from revision control
            # kick off a clean build
            # perform packaging
         when "?"
            showUsage
         end
      end

      puts "project source folder: #{@source_folder}\n"
      puts "params: #{@param_list}"
   end
end

p = ParamParser.new
