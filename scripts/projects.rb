# Author: Rajinder Yadav <safetynet@devmentor.org>
# Date: Feb 8, 2012
# Web : safetynet.devmentor.org

def createKDevelopProject( platform )
   Dir.chdir(KDEVELOP_FOLDER)
   
   if( platform.fLinuxOS )

      generateFlags = "#{$sGenerateFlags} -D UT_LINUX_BUILD=1"
      generateFlags += " -D UT_QT5SDK_BUILD=1" if( $fGenerateWithQt5 && platform.fQT5SDK )
      puts "==> Generating KDevelop3 + GNU Linux make project"
      puts "==> CMake Flags: #{generateFlags}"
      notifyCMakeProject system( %Q!cmake -G "KDevelop3 - Unix Makefiles" #{generateFlags} ../src! )

   end
   Dir.chdir("..")
end

def createEclipseProject( platform )
   Dir.chdir(ECLIPSE_FOLDER)
   
   if( platform.fLinuxOS )

      generateFlags = "#{$sGenerateFlags} -D UT_LINUX_BUILD=1"
      generateFlags += " -D UT_QT5SDK_BUILD=1" if( $fGenerateWithQt5 && platform.fQT5SDK )
      puts "==> Generating Eclipse CDT4 - Unix Makefiles"
      puts "==> CMake Flags: #{generateFlags}"
      notifyCMakeProject system( %Q!cmake -G "Eclipse CDT4 - Unix Makefiles" #{generateFlags} ../src! )

   elsif( platform.fWinOS )

      generateFlags = "#{$sGenerateFlags} -D UT_MINGW_BUILD=1"
      generateFlags += " -D UT_QT5SDK_BUILD=1" if( $fGenerateWithQt5 && platform.fQT5SDK )
      puts "==> Generating Eclipse CDT4 - MinGW make project"
      puts "==> CMake Flags: #{generateFlags}"
      notifyCMakeProject system( %Q!cmake -G "Eclipse CDT4 - MinGW Makefiles" #{generateFlags} ../src! )

   end
   Dir.chdir("..")
end
