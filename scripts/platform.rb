# Author: Rajinder Yadav <safetynet@devmentor.org>
# Date: Jan 15, 2012
# Web : safetynet.devmentor.org

class Platform

   attr_accessor :fLinuxOS
   attr_accessor :fWinOS
   attr_accessor :fMacOS
   attr_accessor :fMinGW
   attr_accessor :fCodeblocks
   attr_accessor :fVisualCPP
   attr_accessor :desktop
   attr_accessor :fQT5SDK
   attr_accessor :fMSSDK

   def initialize
      @fLinuxOS       = false
      @fWinOS         = false
      @fMacOS         = false
      @fMinGW         = false
      @fCodeblocks    = false
      @fVisualCPP     = false
      @desktop        = ""
      @fGrowl         = false
      @fQT5SDK        = false
      @fMSSDK         = false

      @fVisualStudio  = false

      # check OS, IDEs, Compilers
      @fLinuxOS = true if( RUBY_PLATFORM =~ /linux/i || system("uname") =~ /linux/i || ENV["OSTYPE"] =~ /linux/i )
      @fMacOS   = true if( ENV['MACHTYPE'] =~ /apple/i || ENV['_system_name'] =~ /osx/i ||  system("uname") =~ /darwin/i )
      @fWinOS   = true if( ENV["OS"] =~ /win/i || (ENV["PATH"] =~ /program files/i) )
      @fQT5SDK  = true if( ENV['QT_ROOT'] )

      if( @fWinOS ) 
         @fMinGW        = true if( ENV["MINGW_ROOT_DIRECTORY"] != nil || Dir.exists?("c:/mingw") || ENV["PATH"] =~ /mingw/i )
         @fCodeblocks   = true if( Dir.exists?(ENV["ProgramFiles"]+"\\CodeBlocks") )
         @fVisualCPP    = true if( ENV["VCINSTALLDIR"] != nil )
         @fGrowl        = true if( File.exists? "#{ENV['ProgramFiles']}\\Growl for Windows\\growlnotify.exe" )
         @fVisualStudio = true if( ENV["VCINSTALLDIR"] =~ /Visual Studio/i )
         @fMSSDK        = true if( ENV["MSSDK_ROOT"] != nil )
      else
         @fCodeblocks = true if( system("which codeblocks") )
      end

      if( @fWinOS )
         @desktop = @fGrowl ? "win+growl" : "win"
      elsif( ENV["DESKTOP_SESSION"] =~ /kde/i || system( "which kdialog" ) || ENV["KDE_FULL_SESSION"] != nil )
         @desktop = "kde"
      elsif( ENV["DESKTOP_SESSION"] =~ /ubuntu/i )
         @desktop = "linux+notify-send"
      elsif( system( "which notify-send" ) )
         @desktop = "linux+notify-send"
      end
   end

   def ExitWithMessage(msg, err)
      # NOTE: unix tool and script return zero for success!
      # system returns true if the command gives zero exit status, 
      # false for non zero exit status. Returns nil if command execution fails. 
      # An error status is available in $?.
      puts msg
      exit false
   end

   # do a basic check for the build system
   def CheckBuildToolRequirements( fGUI )
      # Check necessary environment variables have been set
      ExitWithMessage( "==> Environment variable SAFETYNET_ROOT needs to be defined, see README.", 1 ) if( ! ENV["SAFETYNET_ROOT"] )
      puts "==> Environment variable QT_ROOT needs to be defined if you plan to use the Unit Test Monitor, see README." if( fGUI && !@fQT5SDK )

      if( @fLinuxOS )
         ExitWithMessage( "==> Missing C++ compiler", 1 ) if( !system("which g++") )
         ExitWithMessage( "==> Missing CMake", 1 ) if( !system("which cmake") )
         puts "==> notify-send missing" if ( @desktop != "debian+notify-send" && @desktop != "kde" )
      elsif( @fWinOS )
         ExitWithMessage( "==> Environment variable MINGW_ROOT_DIRECTORY needs to be defined, see README.", 1 ) if( ( @fQT5SDK || @fMinGW ) && !ENV["MINGW_ROOT_DIRECTORY"] )
         ExitWithMessage( "==> Environment variable MSSDK_ROOT needs to be defined. Install the Windows Platform SDK if you need to, see README.", 1 ) if( @fVisualStudio && !ENV["MSSDK_ROOT"] )
         if( !@fVisualCPP && !(@fQT5SDK || @fMinGW ) )
            ExitWithMessage( "==> You are missing a development environment\nPlease Install Qt5SDK or VisualStudio" )
         end
         puts "==> Growl for Windows missing" if( !Dir.exist?( ENV["ProgramFiles"]+"\\Growl for Windows" ) )
      elsif( @fMacOS )
         ExitWithMessage( "==> Missing C++ compiler", 1 ) if( !system("which clang") )
         ExitWithMessage( "==> Missing CMake", 1 ) if( !system("which cmake") )
      end
   end

end
