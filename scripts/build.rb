#!/usr/bin/env ruby
#
# Author: Rajinder Yadav <safetynet@devmentor.org>
# Date: Jan 19, 2012
# Web : safetynet.devmentor.org

require "fileutils"
require "shell"
require_relative "platform"

# Default - We only build the UnitTest Core and don't (want to) depend on Qt
fBuildGUI = false

# Update check: Do a Git pull
if( ARGV[0] == "update" )
   puts "==> \e[1;42mFetching latest source for SafetyNet\e[0m"
   current_dir = Dir.pwd
   Dir.chdir( ENV["SAFETYNET_ROOT"] )
   system("git pull")
   Dir.chdir( current_dir )
end

# Default - We don't build a GUI to make sure the UnitTest core builds everywhere
# This will determine if build will include GUI UnitTest Monitor
if( ARGV.index( "gui" ) )
   puts "==> Building UnitTest Monitor GUI"
   fBuildGUI = true
else
	puts "==> Building UnitTest Core"
end

# Init Platform and perform checks
platform = Platform.new
platform.CheckBuildToolRequirements( fBuildGUI ) 

# Blow away folders created by build
# TODO: Maybe update code to do a safe clean in case someone copies file into these folders
if( Dir.exist?( ENV["SAFETYNET_ROOT"]+"/Bin" ) )
   FileUtils.rm_rf( ENV["SAFETYNET_ROOT"]+"/Bin" )
end
Dir.mkdir( ENV["SAFETYNET_ROOT"]+"/Bin" )

if( Dir.exist?( ENV["SAFETYNET_ROOT"]+"/UnitTest/Lib" ) )
   FileUtils.rm_rf( ENV["SAFETYNET_ROOT"]+"/UnitTest/Lib" )
end
Dir.mkdir( ENV["SAFETYNET_ROOT"]+"/UnitTest/Lib" ) if( platform.fVisualCPP || platform.fLinuxOS)

if( Dir.exist?( ENV["SAFETYNET_ROOT"]+"/UnitTest/LibMinGW" ) )
   FileUtils.rm_rf( ENV["SAFETYNET_ROOT"]+"/UnitTest/LibMinGW" )
end
Dir.mkdir( ENV["SAFETYNET_ROOT"]+"/UnitTest/LibMinGW" ) if( platform.fMinGW )

# Kick off builds
puts "==> Building project UnitTest"
exit(false) if ( !system( "ruby gen.rb #{ENV["SAFETYNET_ROOT"]}/UnitTest clean g:make g:nmake g:mingw" ) )

puts "==> Copying UnitTest library into lib folder"
if( platform.fWinOS )
   FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/UnitTest/build_nmake/UnitTest.lib", ENV["SAFETYNET_ROOT"]+"/UnitTest/Lib/" ) if( platform.fVisualCPP )
   FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/UnitTest/build_mingw/libUnitTest.dll.a", ENV["SAFETYNET_ROOT"]+"/UnitTest/LibMinGW/" ) if( platform.fMinGW )
else
   FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/UnitTest/build_make/libUnitTest.so", ENV["SAFETYNET_ROOT"]+"/UnitTest/Lib/" )
#   FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/UnitTest/build_make/libUnitTest.a", ENV["SAFETYNET_ROOT"]+"/UnitTest/Lib/" )
end

puts "==> Building project utgen"
exit(false) if ( !system( "ruby gen.rb #{ENV["SAFETYNET_ROOT"]}/utgen clean g:make g:nmake g:mingw" ) )

puts "==> Building project UnitTestSample"
exit(false) if ( !system( "ruby gen.rb #{ENV["SAFETYNET_ROOT"]}/UnitTestSample clean g:make g:nmake g:mingw" ) )

# TODO: Consider removing GUI to seperate project
if( platform.fQT5SDK && fBuildGUI ) 
   puts "==> Building project SafetyNetMonitor"
   exit(false) if( !system( "ruby gen.rb #{ENV["SAFETYNET_ROOT"]}/SafetyNetMonitor clean g:make g:mingw g:qt" ) )

   # Optional, used to testing, not for public consumption
   if( false )
      puts "==> Building project MonitorTestClient"
      exit(false) if ( !system( "ruby gen.rb #{ENV["SAFETYNET_ROOT"]}/MonitorTestClient clean g:make g:nmake g:mingw" ) )
   end
end

if( platform.fWinOS )
   if( platform.fQT5SDK )
      puts "==> Copying Qt5SDK DLLs to Bin folder"
      # copy Qt5 DLLs into Unit Test Monitor mingw build folder
      FileUtils.cp( ENV["QT_ROOT"]+"/lib/QtGui4.dll", ENV["SAFETYNET_ROOT"]+"/Bin")
      FileUtils.cp( ENV["QT_ROOT"]+"/lib/QtCore4.dll", ENV["SAFETYNET_ROOT"]+"/Bin")
      FileUtils.cp( ENV["QT_ROOT"]+"/lib/QtNetwork4.dll", ENV["SAFETYNET_ROOT"]+"/Bin")
   end

   # copy UnitTest DLL
   puts "==> Copying UnitTest DLLs to Bin folder"
   FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/UnitTest/build_nmake/UnitTest.dll", ENV["SAFETYNET_ROOT"]+"/Bin") if( platform.fVisualCPP )
   FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/UnitTest/build_mingw/libUnitTest.dll", ENV["SAFETYNET_ROOT"]+"/Bin") if( platform.fMinGW )
   
   if( fBuildGUI )
      puts "==> Copying project binaries into Bin folder"
      # copy Windows exes into Bin folder
      FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/SafetyNetMonitor/build_mingw/SafetyNetMonitor.exe", ENV["SAFETYNET_ROOT"]+"/Bin") if( platform.fQT5SDK )
   end

   if( platform.fVisualCPP )
      # NMake / Visual C++ build
      FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/UnitTest/build_nmake/test/SafetyNetTest.exe", ENV["SAFETYNET_ROOT"]+"/Bin")
      FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/UnitTestSample/build_nmake/SampleTest.exe", ENV["SAFETYNET_ROOT"]+"/Bin")
      FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/utgen/build_nmake/utgen.exe", ENV["SAFETYNET_ROOT"]+"/Bin")
      
      # For development & debugging
      if( false && fBuildGUI )
         FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/MonitorTestClient/build_nmake/MonitorTestClient.exe", ENV["SAFETYNET_ROOT"]+"/Bin")
      end
   else
      #MinGW Build
      FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/UnitTest/build_mingw/test/SafetyNetTest.exe", ENV["SAFETYNET_ROOT"]+"/Bin")
      FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/UnitTestSample/build_mingw/SampleTest.exe", ENV["SAFETYNET_ROOT"]+"/Bin")
      FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/utgen/build_mingw/utgen.exe", ENV["SAFETYNET_ROOT"]+"/Bin")
      
      # For development & debugging
      if( false && fBuildGUI )
         FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/MonitorTestClient/build_mingw/MonitorTestClient.exe", ENV["SAFETYNET_ROOT"]+"/Bin")
      end
   end
elsif( platform.fLinuxOS || platform.fMacOS )
   # copy Linux exes into Bin folder
   FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/UnitTest/build_make/test/SafetyNetTest", ENV["SAFETYNET_ROOT"]+"/Bin")
   FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/UnitTestSample/build_make/SampleTest", ENV["SAFETYNET_ROOT"]+"/Bin")
   FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/utgen/build_make/utgen", ENV["SAFETYNET_ROOT"]+"/Bin")
   
   if( fBuildGUI )
      FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/SafetyNetMonitor/build_make/SafetyNetMonitor", ENV["SAFETYNET_ROOT"]+"/Bin") if( platform.fQT5SDK )
      # For development & debugging
      if( false )
         FileUtils.cp( ENV["SAFETYNET_ROOT"]+"/MonitorTestClient/build_make/MonitorTestClient", ENV["SAFETYNET_ROOT"]+"/Bin")
      end
   end
   system( "chmod +x #{ENV["SAFETYNET_ROOT"]}/Bin/*")
end

puts "==> Build process completed"
