# Author: Rajinder Yadav <safetynet@devmentor.org>
# Date: Dec 18, 2011
# Web : safetynet.devmentor.org

def putsPass( msg )
   puts $fColorizeText ? "==> \e[1;42m[PASS] #{msg}\e[0m" : "[PASS] #{msg}"
end

def putsFail( msg )
   puts $fColorizeText ? "==> \e[1;41m[FAIL] #{msg}\e[0m" : "[FAIL] #{msg}"
end

def notifBuild( err )
   if( err )
      putsPass "==> Build completed successfully"
   else
      putsFail "==> Build failed"
      $errCode = ERROR_BUILD_FAILED
   end
end

def notifyCMake( err )
   if( err )
      putsPass "==> Generated Makefiles successfully"
   else
      putsFail "==> CMake generate error"
      $errCode = ERROR_CMAKE_MAKEFILE
   end
end

def notifyCMakeProject( err )
   if( err )
      putsPass "==> Generated project successfully"
   else
      putsFail "==> CMake project generate error"
      $errCode = ERROR_CMAKE_PROJECT
   end
end

def notifyExit( err, desktop )
   if( err == 0 )
      putsPass "==> Successfuly completed all tasks"
      system( %Q!kdialog --passivepopup "<b><font size=10 color=green>Build Successful</font></b>" -title "#{ARGV[0]} - Success" 10&! ) if( desktop == "kde" )
      system( %Q!"#{ENV['ProgramFiles']}\\Growl for Windows\\growlnotify.exe" /t:"#{ARGV[0]} - Success" "Build Successful"! ) if( desktop == "win+growl" )
      system( %Q!notify-send "#{ARGV[0]} - Success" "Build Successful"! ) if( desktop == "linux+notify-send" )
   else
      putsFail "==> Not all tasks were completed"
      system( %Q!kdialog --error "<b><font size=10 color=red>Build Failed</font></b>" -title "#{ARGV[0]} - Failed" 180&! ) if( desktop == "kde" )
      system( %Q!"#{ENV['ProgramFiles']}\\Growl for Windows\\growlnotify.exe" /t:"#{ARGV[0]} - Failed" /s:true "Build Failed"! ) if( desktop == "win+growl" )
      system( %Q!notify-send -t 45000 "#{ARGV[0]} - Failed" "Build Failed"! ) if( desktop == "linux+notify-send" )
   end
   exit err
end
