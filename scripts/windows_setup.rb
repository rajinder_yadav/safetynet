# Author: Rajinder Yadav <safetynet@devmentor.org>
# Date: Jan 15, 2012
# Web : safetynet.devmentor.org

def prepareWinBuildFolder( platform )
   # check script argument
   case(ARGV[1])
   when "clean"
      # delete all build, cmake generated folders (for clean re-generation)
      puts "==> Deleting all projects build folders"
      FileUtils.rm_rf(BUILD_FOLDER)       if( Dir.exist?(BUILD_FOLDER) )
      FileUtils.rm_rf(VCPP_FOLDER)        if( Dir.exist?(VCPP_FOLDER) )
      FileUtils.rm_rf(MINGW_FOLDER)       if( Dir.exist?(MINGW_FOLDER) )
      FileUtils.rm_rf(CODEBLOCKS_FOLDER)  if( Dir.exist?(CODEBLOCKS_FOLDER) )
      FileUtils.rm_rf(ECLIPSE_FOLDER)     if( Dir.exist?(ECLIPSE_FOLDER) )

   when "build"
      # only delete Makefile build folders (for clean build)
      puts "==> Deleting build folder"
      FileUtils.rm_rf(BUILD_FOLDER)
      FileUtils.rm_rf(MINGW_FOLDER)
   when "?"
      showUsage
      exit false
   end

   if( $fGenerateNMame && platform.fVisualCPP && !Dir.exist?(BUILD_FOLDER) )
      puts "==> Creating NMake Makefile build folder"
      Dir.mkdir(BUILD_FOLDER)
   end

   if( $fGenerateMinGW && platform.fMinGW && !Dir.exist?(MINGW_FOLDER) )
      puts "==> Creating MinGW Makefile build folder"
      Dir.mkdir(MINGW_FOLDER)
   end

   if( $fGenerateVCPP && platform.fVisualCPP && !Dir.exist?(VCPP_FOLDER) )
      puts "==> Creating VisualStudio project folder"
      Dir.mkdir(VCPP_FOLDER)
   end

   if( $fGenerateCodeBlocks && platform.fCodeblocks && !Dir.exist?(CODEBLOCKS_FOLDER) )
      puts "==> Creating CodeBlocks project folder"
      Dir.mkdir(CODEBLOCKS_FOLDER)
   end

   if( $fGenerateEclipse && !Dir.exist?(ECLIPSE_FOLDER) )
      puts "==> Creating Eclipse Makefile build folder"
      Dir.mkdir(ECLIPSE_FOLDER)
   end
end

def createMinGWMakefiles( platform ) return if( !$fGenerateMinGW )
   generateFlags = "#{$sGenerateFlags} -D UT_MINGW_BUILD=1"
   generateFlags += " -D UT_QT5SDK_BUILD=1" if( $fGenerateWithQt5 && platform.fQT5SDK )
   Dir.chdir(MINGW_FOLDER)
   puts "==> Generating Windows MinGW Makefiles"
   puts "==> CMake Flags: #{generateFlags}"
   notifyCMake system( %Q!cmake -G "MinGW Makefiles" #{generateFlags} ../src! )
   Dir.chdir("..")
end

def createNMakefiles( platform )
   return if( !$fGenerateNMame )
   generateFlags = "#{$sGenerateFlags} -D UT_NMAKE_BUILD=1"
   Dir.chdir(BUILD_FOLDER)
   puts "==> Generating Windows VC++ NMake Makefiles"
   puts "==> CMake Flags: #{generateFlags}"
   notifyCMake system( %Q!cmake -G "NMake Makefiles" #{generateFlags} ../src! )
   Dir.chdir("..")
end

def createVisualCPPProject( platform )
   return if( !$fGenerateVCPP )
   generateFlags = "#{$sGenerateFlags} -D UT_NMAKE_BUILD=1"
	Dir.chdir(VCPP_FOLDER)
   if( ENV["VCINSTALLDIR"] =~ /Visual Studio 10/i )
     puts "==> Generating VisualStudio 10 project"
	  notifyCMakeProject system( %Q!cmake -G "Visual Studio 10" #{generateFlags} ../src! )
   elsif( ENV["VCINSTALLDIR"] =~ /Visual Studio 11/i )
     puts "==> Generating VisualStudio 11 project"
     notifyCMakeProject system( %Q!cmake -G "Visual Studio 11" #{generateFlags} ../src! )
   elsif( ENV["VCINSTALLDIR"] =~ /Visual Studio 9/i )
     puts "==> Generating VisualStudio 9 project"
     notifyCMakeProject system( %Q!cmake -G "Visual Studio 9 2008" #{generateFlags} ../src! )
   elsif( ENV["VCINSTALLDIR"] =~ /Visual Studio 8/i )
     puts "==> Generating VisualStudio 8 project"
     notifyCMakeProject system( %Q!cmake -G "Visual Studio 8 2005" #{generateFlags} ../src! )
   elsif( ENV["VCINSTALLDIR"] =~ /Visual Studio 7/i )
     puts "==> Generating VisualStudio 7 project"
     notifyCMakeProject system( %Q!cmake -G "Visual Studio 7" #{generateFlags} ../src! )
   end
   puts "==> CMake Flags: #{generateFlags}"
	Dir.chdir("..")
end

def buildWinBinaries( platform )
   if( platform.fVisualCPP && $fGenerateNMame )
      Dir.chdir(BUILD_FOLDER)
      puts "==> Building with VC++ NMake"
      notifBuild system 'nmake'
      Dir.chdir("..")
   end
   if( platform.fMinGW && $fGenerateMinGW )
      Dir.chdir(MINGW_FOLDER)
      puts "==> Building with MinGW make"
      notifBuild system 'mingw32-make'
      Dir.chdir("..")
   end
end
