# ================================================================================
# Apache License Version 2.0
# 
# Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ================================================================================

cmake_minimum_required (VERSION 3.2)
project (MonitorTestClient)

if(UT_NMAKE_BUILD)

   # Define this if you don't want to link with Qt SDK (will use WinSock)
   message("Setting up Windows NMake configurations and winsock2 linkage")
   SET(CMAKE_CXX_FLAGS "-D WINDOWS_BUILD")
   include_directories("$ENV{MSSDK_ROOT}/Include")
   link_directories("$ENV{MSSDK_ROOT}/Lib")
   set(UT_WINSOCK_LIB WS2_32.lib)

elseif(UT_LINUX_BUILD)

   # Define this, if you don't want to link with Qt SDK (will use tcp/ip sockets)
   message("Settings up Linux Make configuration and linkage")
   SET(CMAKE_CXX_FLAGS "-D LINUX_BUILD")

endif()

if(UT_QT5SDK_BUILD)

   # Find includes in corresponding build directories
   set(CMAKE_INCLUDE_CURRENT_DIR ON)

   # Instruct CMake to run moc automatically when needed.
   set(CMAKE_AUTOMOC ON)

   # Find the QtWidgets library
   find_package(Qt5Widgets)
   find_package(Qt5Core)
   get_target_property(QtCore_location Qt5::Core LOCATION)
   qt5_use_modules(SafetyNetMonitor Network)

endif()

set(UnitClient_Source main.cpp)
add_executable(MonitorTestClient ${UnitClient_Source})
target_link_libraries(MonitorTestClient ${UT_WINSOCK_LIB})
