/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

#include <cstdlib>
#include <iostream>
#include <cstring>
#include <cstdio>

#ifdef QTSDK_BUILD
	#include <QtNetwork/QAbstractSocket>
	#include <QtNetwork/QUdpSocket>
	#include <QtNetwork/QHostAddress>
	#include <QtCore/QByteArray>
#elif LINUX_BUILD
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <unistd.h>
#elif WINDOWS_BUILD
	#define WIN32_LEAN_AND_MEAN      // Exclude rarely-used stuff from Windows headers
	#include <windows.h>
	#include <Winsock2.h>
#endif

#ifdef _sleep
	#undef _sleep
#endif

// note _WIN32 is defined by MinGW
#if defined ( _WINDOWS ) || defined( _WIN32)
	// Windows sleep is in milli-sec, turn this into a no-op for now!
	#define _sleep(v)
#else
	// Linux sleep is in micro-sec
	#define _sleep(v) usleep(v)
#endif

using namespace std;

int main( int argc, char** argv )
{
	const int BUF_SIZE = 1024;
	char buf[BUF_SIZE] = {0};
	std::string msg( "This is being send from the client, it's a long line to test basic UDP server capacity and response handling!");

	int sendCount = 1;
	if( argc >= 2 )
	{
		sendCount = atoi(argv[1] );
	}
	if( argc > 2 )
	{
		msg.clear();
		for(int i=2; i < argc; ++i)
		{
			if( i >= 2 ) msg += " ";
			msg += argv[i];
		}
	}
	const unsigned short MONITOR_PORT = 61371;

#ifdef QTSDK_BUILD

	QUdpSocket* m_udpSocket = m_udpSocket = new QUdpSocket;
	QHostAddress udpServer(QHostAddress::LocalHost);
	QByteArray packet;

	for( int i=0; i < sendCount; ++i )
	{
		sprintf( buf, "%i) %s", i, msg.c_str() );
		packet = buf;

		m_udpSocket->writeDatagram( packet,
			packet.size(),
			udpServer,
			MONITOR_PORT );

		// this sleep is very crucial to prevent corruption of receiver's buffer
		_sleep( 250 );
	}

	m_udpSocket->close();
	delete m_udpSocket;


#elif LINUX_BUILD

	// get a socket
	int sd = socket( AF_INET, SOCK_DGRAM, 0 );
	if( sd < 0 ) {
		cout << "Socket error\n";
		return -1;
	}

	// assign server socket address
	sockaddr_in sa_server;
	memset( &sa_server, 0, sizeof( sockaddr_in) );
	sa_server.sin_family = AF_INET;
	sa_server.sin_port   = htons( 61371 ); // day-time server;
	inet_aton( "127.0.0.1", &sa_server.sin_addr );

	if( connect( sd, (sockaddr*)&sa_server, sizeof(sa_server) ) < 0 ) {
		cout << "Connection error\n";
		return -1;
	}

	for( int i = 0; i < sendCount; ++i )
	{
		sprintf( buf, "%i) %s", i, msg.c_str() );
		write( sd, buf, BUF_SIZE );
		_sleep( 250 );
	}
	close( sd );

#elif WINDOWS_BUILD

	// Initialize WinSock2.2 DLL
	// low word = major, highword = minor
	WSADATA wsaData = {0};
	WORD wVer = MAKEWORD(2,2);

	int nRet = WSAStartup( wVer, &wsaData );
	if( nRet == SOCKET_ERROR ) 
	{
		cout << "Failed to init Winsock library" << endl;
		return -1;
	}

	WORD WSAEvent = 0;
	WORD WSAErr   = 0;

	SOCKET hServer  = {0};

	// open a socket
	//
	// for the server we do not want to specify a network address
	// we should always use INADDR_ANY to allow the protocal stack
	// to assign a local IP address
	hServer = socket( AF_INET, SOCK_DGRAM, 0 );

	if( hServer == INVALID_SOCKET ) 
	{
		cout << "Invalid socket, failed to create socket" << endl;
		return -1;
	}

	// name a socket
	sockaddr_in saServer = {0};

	saServer.sin_family      = PF_INET;   
	saServer.sin_port        = htons( 61371 );           // well-known ports defined in RFC 1700                                    
	saServer.sin_addr.s_addr = inet_addr( "127.0.0.1" ); // localhost

	if( nRet == SOCKET_ERROR ) 
	{
		cout << "Connection to server failed" << endl;
		closesocket( hServer );
		return -1;
	}

	for( int i=0; i < sendCount; ++i )
	{
		sprintf( buf, "%i) %s", i, msg.c_str() );
		sendto( hServer, buf, strlen(buf), 0, (sockaddr*)&saServer, sizeof(sockaddr_in) );
	}

	// close server socket
	nRet = closesocket( hServer );
	hServer = 0;

	nRet = WSACleanup();
	if( nRet == SOCKET_ERROR ) 
	{
		cout << "Error cleaning up Winsock Library" << endl;
		return -1;
	}

#endif

return 0;
}

#if 0
int sendTCP(const int sendCount, std::string* msg)
{
	for( int i=0; i < sendCount; ++i ) {
		// get a socket
		int sd = socket( AF_INET, SOCK_STREAM, 0 );
		if( sd < 0 ) {
			cout << "Socket error\n";
			return -1;
		}

		// assign server socket address
		sockaddr_in sa_server;
		memset( &sa_server, 0, sizeof( sockaddr_in) );
		sa_server.sin_family = AF_INET;
		sa_server.sin_port   = htons( 61371 ); // day-time server;
		inet_aton( "127.0.0.1", &sa_server.sin_addr );

		if( connect( sd, (sockaddr*)&sa_server, sizeof(sa_server) ) < 0 ) {
			cout << "Connection error\n";
			return -1;
		}

		int n = 0;
		const int BUF_SIZE = 1024;
		char buf[BUF_SIZE + 1] = {};
		//char* pBuf = buf;
		//int bufLen = BUF_SIZE;

		char* pBuf = buf;
		int bufLen = BUF_SIZE;
		strcpy(buf, msg.c_str());

		while(true)
		{
			n = write( sd, pBuf, bufLen );
			if( n <= 0 || bufLen <= 0 ) break;
			pBuf += n;
			bufLen -= n;
		}
		close( sd );
	}

	return 0;
}
#endif
