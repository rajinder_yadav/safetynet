/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

#ifndef UDPMonitor_H
#define UDPMonitor_H

#include <QAbstractSocket>

class QUdpSocket;

class UDPMonitor : public QObject
{
    Q_OBJECT

    QUdpSocket* m_udpSocket;

    static const unsigned short MONITOR_PORT = 61371;
public:
    explicit UDPMonitor();

    int m_sServer;
    int m_sClient;

signals:
    void postTestResult( const char* msg );

public slots:
    void startServer();
    void handleClientRequest();
    void udpSocketError( QAbstractSocket::SocketError err );
};

#endif // UDPMonitor_H
