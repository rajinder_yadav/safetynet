/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

#include "unittestmonitordialog.h"
#include "udpmonitor.h"
#include "ui_unittestmonitordialog.h"
#include <QThread>
#include <queue>

//#include <sys/types.h>
//#include <sys/socket.h>
//#include <netinet/in.h>

UnitTestMonitorDialog::UnitTestMonitorDialog( QWidget *parent ) :
    QDialog( parent ),
    ui( new Ui::UnitTestMonitorDialog )
{
    ui->setupUi(this);

    /// running inside another thread seem to get data lost???
#if 1
    m_udpMonitor  = new UDPMonitor;

    connect( m_udpMonitor,
             SIGNAL( postTestResult(const char*) ),
             this,
             SLOT( showTestResult(const char*) ) );
    m_udpMonitor->startServer();
#else
    m_pServerThread = new QThread;
    m_udpMonitor    = new UDPMonitor;

    connect( m_udpMonitor,
             SIGNAL( postTestResult(const char*) ),
             this,
             SLOT( showTestResult(const char*) ) );

    connect( m_pServerThread,
             SIGNAL( started() ),
             m_udpMonitor,
             SLOT( startServer() ) );


    m_udpMonitor->moveToThread( m_pServerThread );
    m_pServerThread->start();
#endif

    connect( ui->btnClear,
             SIGNAL( clicked() ),
             this,
             SLOT( clearMonitorDisplay()) );
}

UnitTestMonitorDialog::~UnitTestMonitorDialog()
{
    // we can't wait on the thread because we will hang since
    // server is in a infinite loop and blocked on accept socket call
//    m_pServerThread->exit();
    delete m_udpMonitor;
//    delete m_pServerThread;
    delete ui;
}

void UnitTestMonitorDialog::clearMonitorDisplay()
{
    ui->textUnitTestResults->clear();
}

void UnitTestMonitorDialog::showTestResult( const char* msg )
{
    ui->textUnitTestResults->append( msg );
}
