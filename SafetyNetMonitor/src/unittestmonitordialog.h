/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

#ifndef UNITTESTMONITORDIALOG_H
#define UNITTESTMONITORDIALOG_H

#include <QDialog>

class UDPMonitor;

namespace Ui
{
    class UnitTestMonitorDialog;
}

class UnitTestMonitorDialog : public QDialog
{
    Q_OBJECT
    
    UDPMonitor* m_udpMonitor;
    //QThread* m_pServerThread;

public:
    explicit UnitTestMonitorDialog( QWidget *parent = 0 );
    ~UnitTestMonitorDialog();

private:
    Ui::UnitTestMonitorDialog *ui;

    unsigned int m_testCase;
    unsigned int m_testRun;
    unsigned int m_testFailed;
    unsigned int m_testExceptions;

public slots:
    void showTestResult( const char* msg );
    void clearMonitorDisplay();
};

#endif // UNITTESTMONITORDIALOG_H
