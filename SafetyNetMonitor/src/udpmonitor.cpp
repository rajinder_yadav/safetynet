/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

#include "udpmonitor.h"
#include <QUdpSocket>
#include <QHostAddress>
#include <QAbstractSocket>

//#include <sys/types.h>
//#include <sys/socket.h>
//#include <netinet/in.h>

UDPMonitor::UDPMonitor() : QObject(0)
{
}


void UDPMonitor::startServer()
{
    m_udpSocket = new QUdpSocket( this );
    m_udpSocket->bind( QHostAddress::LocalHost, 61371 );

    connect( m_udpSocket,
             SIGNAL( readyRead() ),
             this,
             SLOT( handleClientRequest() ) );

    connect( m_udpSocket,
             SIGNAL( error(QAbstractSocket::SocketError) ),
             this,
             SLOT( udpSocketError(QAbstractSocket::SocketError) ) );

#if 0
    sockaddr_in sin;

    memset( &sin, 0, sizeof(sockaddr_in) );
    sin.sin_family  = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port = htons( (unsigned short)MONITOR_PORT );

    m_sServer = socket( PF_INET, SOCK_DGRAM, 0 );

    if( bind( m_sServer, (sockaddr*)&sin, sizeof(sin)) < 0 )
    {
        // \todo handle error
    }

    socklen_t  addressLength;
    sockaddr_in sinClient;

    const int bufferSize = 1024;
    char buffer[bufferSize];
    memset( buffer, 0, bufferSize );

    while( true )
    {
        recvfrom( m_sServer,
                  buffer,
                  bufferSize,
                  0,
                  (sockaddr*)&sinClient,
                  &addressLength );

        emit postTestResult( buffer );
    }
#endif
}

void UDPMonitor::handleClientRequest()
{
    while( m_udpSocket->hasPendingDatagrams() )
    {
        QHostAddress sender;
        quint16 senderPort;
        QByteArray datagram;

        datagram.resize( m_udpSocket->pendingDatagramSize() );


        m_udpSocket->readDatagram( datagram.data(),
                                   datagram.size(),
                                   &sender,
                                   &senderPort );

        emit postTestResult( static_cast<const char*>( datagram.data() ) );
    }
}


void UDPMonitor::udpSocketError( QAbstractSocket::SocketError err )
{
    switch( err )
    {
        /// An error occurred with the network (e.g., the network cable was accidentally plugged out).
    case QAbstractSocket::NetworkError:
        break;

        /// The address specified to QUdpSocket::bind() is already in use and was set to be exclusive.
    case QAbstractSocket::AddressInUseError:
        break;

    case QAbstractSocket::SocketAddressNotAvailableError:
        break;
        /// The address specified to QUdpSocket::bind() does not belong to the host.
    case QAbstractSocket::DatagramTooLargeError:
        break;

        /// The datagram was larger than the operating system's limit (which can be as low as 8192 bytes).
    case QAbstractSocket::SocketResourceError:
        break;

       /// The socket operation failed because the application lacked the required privileges.
    case QAbstractSocket::SocketAccessError:
        break;

        /// An unidentified error occurred.
    case QAbstractSocket::UnknownSocketError:
        break;

        /// Unhandeled socket error
    default:
        break;
    }
}
