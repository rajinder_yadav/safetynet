/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

// Source: ParseTokenValue.cpp
// Author: Rajinder Yadav
// Date:   July 2, 2004
//
// web:   http://devmentor.org
// email: safetynet@devmentor.org

#include "stdafx.h"
#include "ParseTokenValue.h"
#include "ExceptionParameterInvalid.h"

using std::cout;
using std::endl;

#if defined( _WINDOWS ) || defined( _WIN32 )
   #include <Windows.h>
   const char* OS_PATH_SEPARATOR = "\\";
#else
   #include <cstdlib>
   #include <sys/stat.h>
   const char* OS_PATH_SEPARATOR = "/";
#endif

const std::string ParseTokenValue::sm_strTokenSwitch[TOKEN_COUNT] =
{
   "/a", "/s", "/n", "/o", "/c", "/t", "/project", "/help", "/?","/factory"
};

ParseTokenValue::ParseTokenValue( int argc, const char* argv[] )
{
   if( argc == 1 ) {
      throw ExceptionParameterInvalid();
   }
   
   m_strOutputDir  = ".";
   m_strOutputDir += OS_PATH_SEPARATOR;

   m_eGenerate = UT_TESTCLASS;  // default builds test case classses

   // tokenize arguments passed to the process
   std::string strToken;
   for( int i=1; i < argc; ++i )
   {
      strToken = static_cast<std::string>( argv[i] );
      m_TokenList.push_back( strToken );
   }

   std::vector<std::string>::iterator it = m_TokenList.begin();

   while( it != m_TokenList.end() )
   {
      strToken = static_cast<std::string>( *it );

      // check if we're set to read at a switch
      // if not, the input parameter is invalid 
      if( IsSwitch( strToken ) == false )
      {
         throw ExceptionParameterInvalid();
      }
      ++it; // advanced to token value

      // parse token list based on supported switches
      if( strToken == "/n" )
      {
         ClassNameIs( it );
      }
      else if( strToken == "/s" )
      {
         SubjectIs( it );
      }
      else if( strToken == "/a" )
      {
         AuthorIs( it );
      }
      else if( strToken == "/o" )
      {
         OutputDirIs( it );
      }
      else if( strToken == "/c" )
      {
         CopyrightIs( it );
      }
      else if( strToken == "/t" )
      {
         TestCaseIs( it );
      }
      else if( strToken == "/project" )
      {
         m_eGenerate = UT_CMAKE;
         GenerateCMakeLists( it );
      }
      else if( strToken == "/help" )
      {
         #if defined( _WINDOWS ) || defined( _WIN32 )
            ShellExecute( 0, 
                          "open", 
                          "http://safetynet.devmentor.org/documents/utgen_usage.html",
                          0,
                          0,
                          SW_SHOWNORMAL );
            exit(0);
         #else
            struct stat buf;
            if( stat( "/usr/bin/sensible-browser", &buf ) != -1 ) {
               system( "sensible-browser http://safetynet.devmentor.org/documents/utgen_usage.html" );
               exit(0);
            }
            throw ExceptionParameterInvalid();
         #endif
      }
      else if( strToken == "/factory" )
      {
         m_eGenerate = UT_FACTORYCLASS;
         GenerateFactoryCode( it );
      }
      else if( strToken == "/?" )
      {
         // shows usage in console
         throw ExceptionParameterInvalid();
      }
      else
      {
         cout << "\nParameter error, unknown switch or invalid argument." << endl;
         throw ExceptionParameterInvalid();
      }
      ++it;
   } // while

   // default generated class name
   if( m_strClassName.empty() && !m_strSubject.empty() ) {
      m_strClassName = m_strSubject + "_UnitTest";
   }
}

void ParseTokenValue::ValidateTokenList( std::vector<std::string>::iterator& it ) const
{
   if( it == m_TokenList.end() ) {
      throw ExceptionParameterInvalid();
   }
}


void ParseTokenValue::ClassNameIs( std::vector<std::string>::iterator& it )
{   
   ValidateTokenList( it ); // throw exception on empty parameter value

   // token following the switch should not be another switch
   std::string strValue = static_cast<std::string>( *it );

   if( IsSwitch (strValue ) ) {
      throw ExceptionParameterInvalid();
   }
   m_strClassName = strValue;
}

void ParseTokenValue::SubjectIs( std::vector<std::string>::iterator& it )
{
   ValidateTokenList( it ); // throw exception on empty parameter value

   // token following the switch should not be another switch
   std::string strValue = static_cast<std::string>( *it );

   if( IsSwitch( strValue ) ) {
      throw ExceptionParameterInvalid();
   }
   m_strSubject = strValue;
}

void ParseTokenValue::AuthorIs( std::vector<std::string>::iterator& it )
{
   ValidateTokenList( it ); // throw exception on empty parameter value

   // token following the switch should not be another switch
   std::string strValue = static_cast<std::string>( *it );

   if( IsSwitch( strValue ) ) {
      throw ExceptionParameterInvalid();
   }
   
   m_strAuthor += strValue;
   ++it;

   while( it != m_TokenList.end() )
   {
      strValue = static_cast<std::string>( *it );

      if( IsSwitch( strValue ) ) {
         // exit on next switch
         break;
      }
      m_strAuthor += " ";
      m_strAuthor += strValue;
      ++it;
   } // while
   --it;
}

void ParseTokenValue::OutputDirIs( std::vector<std::string>::iterator& it )
{
   ValidateTokenList( it ); // throw exception on empty parameter value

   // token following the switch should not be another switch
   std::string strValue = static_cast<std::string>( *it );

   if( IsSwitch( strValue ) ) {
      throw ExceptionParameterInvalid();
   }
   m_strOutputDir = strValue + OS_PATH_SEPARATOR;
}

void ParseTokenValue::CopyrightIs( std::vector<std::string>::iterator& it )
{
   ValidateTokenList( it ); // throw exception on empty parameter value

   // token following the switch should not be another switch
   std::string strValue = static_cast<std::string>( *it );

   if( IsSwitch( strValue ) ) {
      throw ExceptionParameterInvalid();
   }
   m_strCopyright += strValue;
   ++it;

   while( it != m_TokenList.end() )
   {
      strValue = static_cast<std::string>( *it );
      if( IsSwitch( strValue ) ) {
         // exit on next switch
         break;
      }

      m_strCopyright += " ";
      m_strCopyright += strValue;
      ++it;
   } // while
   --it;
}

void ParseTokenValue::TestCaseIs( std::vector<std::string>::iterator& it )
{
   ValidateTokenList( it ); // throw exception on empty parameter value

   // token following the switch should not be another switch
   std::string strValue = static_cast<std::string>( *it );

   if( IsSwitch( strValue ) ) {
      throw ExceptionParameterInvalid();
   }   
   m_UnitTestList.push_back( strValue );
   ++it;

   while( it != m_TokenList.end() )
   {
      strValue = static_cast<std::string>( *it );
      if( IsSwitch( strValue ) ) {
         // exit on next switch
         break;
      }
      m_UnitTestList.push_back( strValue );
      ++it;
   }
   --it;
}


void ParseTokenValue::ShowUsage()
{
   cout 
   << "+-----------------------------------------------------------+\n"
   << "| utgen.exe - Unit Test Generator, Version 1.5              |\n"
   << "|                                                           |\n"
   << "| Developed by Rajinder Yadav (c) 2007, 2012                |\n"
   << "|                                                           |\n"
   << "| This utility generates C++ source and header files for    |\n"
   << "| unit testing a <subject> class and also a test project.   |\n"
   << "|                                                           |\n"
   << "| web:   http://safetynet.devmentor.org                     |\n"
   << "| email: safetynet@devmentor.org                            |\n"
   << "+-----------------------------------------------------------+\n"
   << endl
   << "The utgen utility is a tool for generating:" << endl
   << endl
   << "  * Test class" << endl
   << "  * Test project" << endl
   << endl
   << "Creating a test class and a test project is a two step process. Initially, one creates all the test classes that will be needed, then the test project is created. More test classes may be added to the test project later as required." << endl
   << endl
   << "(*) It is important that you follow the convention of creating a sub-folder called 'test' inside the project to be tested." << endl
   << endl
   << "Test Class Generation" << endl
   << "=====================" << endl
   << endl
   << "The switches that can be used to when creating a test class are shown below." << endl
   << endl
   << "/s <Subject class>    [required] the class to be unit tested" << endl
   << "/t <test case> ...    [required] test method names" << endl
   << endl
   << "/n <test class name>  [optional] output unit test class" << endl
   << "/o <output directory> [optional] output file location" << endl
   << "/a <Author name>      [optional] your name" << endl
   << "/c <Copyright>        [optional] copyright notice" << endl
   << endl
   << "/? display usage to console" << endl
   << endl
   << "/help will open up this usage reference in the browser" << endl
   << endl
   << "The two required switch '/s' and '/t' will be the ones you will work most with." << endl
   << "utgen /s <Subject class> /t <test method> ..." << endl
   << endl
   << "The '/s' switch is used to declare the name of the (Subject) class to be tested. Each test class must be generated on it's own, you cannot declare multiple test Subject classes." << endl
   << endl
   << "The '/t' switch is used to declare one or more test method, utgen will post-fix the test method name with '_UT'. If the Subject class has a method 'open', passing utgen, '/t open' will create a test method called 'open_UT'." << endl
   << endl
   << "When the test class is generated, it will use the name of the Subject class that was passed after the '/s' switch and append '_UnitTest'. If you want to give the resulting test class a different name, you can use the '/n' switch to do this." << endl
   << endl
   << "Once a set of test classes have been generated, a test project is created using utgen." << endl
   << endl
   << "Test Project Generation" << endl
   << "=======================" << endl
   << endl
   << "When utgen creates a test project, it will enumerate all the header and source files inside the test project folder and add them to the Makefile. This is why the test project must be created following the generation of the test classes. To generate a test project, use the following command:" << endl
   << "utgen /project <binary name> <test folder> test_source.cpp ..." << endl
   << endl
   << "The <binary name> following /project switch is the name to be given to the test binary. The <test folder> is the path to the 'test' sub-project. What follows next is one or more test (Subject) source file, these files will exists the parent folder of the test project." << endl
   << endl
   << "Two files will be created, the test project main source file used to run the unit test, and a CMake file that can be used to create a Makefile to build the test project and also be used to generate IDE projects." << endl
   << endl;
}

void ParseTokenValue::ValidateUTResults() const
{
   // throw an exception if required fields are missing
   if( m_UnitTestList.empty() ||
       m_strSubject.empty() )
   {
      throw ExceptionParameterInvalid();   
   }
}

void ParseTokenValue::GenerateFactoryCode( std::vector<std::string>::iterator& it )
{
   while( it != m_TokenList.end() )
   {
      std::string strValue = static_cast<std::string>( *it );
      if( IsSwitch( strValue ) ) {
         break;
      }
      else if( strValue.find( "=" ) == std::string::npos ) { 
         throw ExceptionParameterInvalid();
      }
      m_FactoryList.push_back( strValue );
      ++it;
   }
   --it;
}

void ParseTokenValue::GenerateCMakeLists( std::vector<std::string>::iterator& it )
{
   while( it != m_TokenList.end() )
   {
      std::string strParams = static_cast<std::string>( *it );
      if( IsSwitch( strParams ) ) {
         throw ExceptionParameterInvalid();
         //break;
      }
      m_ProjectParams.push_back( strParams );
      ++it;
   }
   --it;
   if( m_ProjectParams.size() < 2 ) {
      throw ExceptionParameterInvalid();
   }
}
