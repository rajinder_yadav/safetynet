/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

/** \brief 

   \file TestProjectOFStream.cpp

   \author Rajinder Yadav
   \date:  Jan 31, 2012

   web:   http://devmentor.org
   email: safetynet@devmentor.org
*/

#include "stdafx.h"
#include "ExceptionParameterInvalid.h"
#include "ParseTokenValue.h"
#include "TestProjectOFStream.h"

#include <cstring>
#include <vector>
#include <string>

#if defined( _WIN32 ) || defined( _WINDOWS ) || defined( __MINGW32__ )
      #include <Windows.h>
#else
      #include <sys/types.h>
      #include <sys/stat.h>
      #include <dirent.h>
#endif

using std::endl;

TestProjectOFStream::TestProjectOFStream( const ParseTokenValue& tokens )
:m_tokenValue( tokens )
{
   const int count = m_tokenValue.ProjectParamSize();
   if( count < 3 ) {
         throw ExceptionParameterInvalid();
   }

   m_testBinaryName  = m_tokenValue.ProjectParam( 0 );
   m_testPath        = m_tokenValue.ProjectParam( 1 );

   for( int i = 2; i < count; ++i )
   {
      m_subjectFiles.push_back( m_tokenValue.ProjectParam( i ) );
   }  
}

void TestProjectOFStream::GenerateCMakeLists()
{
   std::string strSourceFiles;
   std::string strSubjectFiles;
   std::string strHeaderFiles;

   size_t count = m_sourceFiles.size();

   for( size_t i=0; i < count; ++i )
   {
      if( i > 0 ) strSourceFiles += " ";
      strSourceFiles += m_sourceFiles[i];
   }

   count = m_headerFiles.size();

   for( size_t i=0; i < count; ++i )
   {
      if( i > 0 ) strHeaderFiles += " ";
      strHeaderFiles += m_headerFiles[i];
   }

   count = m_subjectFiles.size();

   for( size_t i=0; i < count; ++i )
   {
      strSubjectFiles += " ../";
      strSubjectFiles += m_subjectFiles[i];
   }
   

   m_ofsCMake.open( "CMakeLists.txt" );
   if( m_ofsCMake.is_open() )
   {
      m_ofsCMake
      << "cmake_minimum_required (VERSION 3.2)" << endl
      << "project(" << m_testBinaryName << ")" << endl
      << endl
      << "include_directories(\"${PROJECT_SOURCE_DIR}\")" << endl
      << "include_directories(\"${PROJECT_SOURCE_DIR}/..\")" << endl
      << "include_directories(\"$ENV{SAFETYNET_ROOT}/UnitTest/Include\")" << endl
      << endl
      << "if(UT_NMAKE_BUILD)" << endl
      << endl
      << "   message(\"Setting up Windows NMake configurations and winsock2 linkage\")" << endl
      << "   SET(CMAKE_CXX_FLAGS \"-D WINDOWS_BUILD\")" << endl
      << "   include_directories(\"$ENV{MSSDK_ROOT}/Include\")" << endl
      << "   link_directories(\"$ENV{MSSDK_ROOT}/Lib\")" << endl
      << "   set(UT_WINSOCK_LIB WS2_32.lib)" << endl
      << endl
      << "elseif(UT_LINUX_BUILD)" << endl
      << endl
      << "   message(\"Settings up Linux Make configuration and linkage\")" << endl
      << "   SET(CMAKE_CXX_FLAGS \"-D LINUX_BUILD\")" << endl
      << endl
      << "endif()" << endl
      << endl
      << "# Important! MinGW linker search path must be listed first" << endl
      << "# Important! followed by NMake linker search path, otherwise MinGW builds will fail" << endl
      << "link_directories(\"$ENV{SAFETYNET_ROOT}/UnitTest/LibMinGW\" \"$ENV{SAFETYNET_ROOT}/UnitTest/Lib\")" << endl
      << endl
      << "#set(TestSubjectClasses" << strSubjectFiles << ")" << endl
      << endl
      << "set(SafetyNetTest_Header " << strHeaderFiles << ")" << endl
      << endl
      << "set(SafetyNetTest_Source TestMain.cpp " << strSourceFiles << " ${TestSubjectClasses})" << endl
      << endl
      << "add_executable(" << m_testBinaryName << " ${SafetyNetTest_Source} ${SafetyNetTest_Header})" << endl
      << "target_link_libraries(" << m_testBinaryName << " UnitTest ${UT_WINSOCK_LIB})" << endl;
   }
   m_ofsCMake.flush();
   m_ofsCMake.close();
}

void TestProjectOFStream::GenerateMain()
{
   m_ofsCMake.open( "TestMain.cpp" );      
   if( m_ofsCMake.is_open() )
   {
      m_ofsCMake
      << "/** \\brief Unit test project generated by SafetyNet" << endl
      << endl
      << "   To obtain your copy of SafetyNet visit http://safetynet.devmentor.org" << endl
      << endl
      << "   you can obtain the latest copy from GITORIOUS using git," << endl
      << endl      
      << "   git clone git@gitorious.org:safetynet/safetynet.git" << endl
      << endl
      << "   web:   http://devmentor.org" << endl
      << "   email: <safetynet@devmentor.org>" << endl
      << endl
      << "*/" << endl
      << endl      
      << "#include <UnitTest.h>" << endl
      << endl
      << "int main(int /*argc*/, char** /*argv[]*/)" << endl
      << "{" << endl
      << endl
      << "   // setup console logging" << endl
      << "   ConsoleLogger display;" << endl
      << endl
      << "   // setup test result observer" << endl
      << "   DECLARE_OBSERVER( display );" << endl
      << endl
      << "   // begin unit testing" << endl
      << "   // NOTE: GUI Test Monitor and file logger observer are" << endl
      << "   //       created when we start the unit test with this macro" << endl
      << "   START_UNIT_TEST( \"UnitTest.log\" );" << endl
      << "   return 0;" << endl
      << "}" << endl
      << endl;
   }
   m_ofsCMake.flush();
   m_ofsCMake.close();
}

bool TestProjectOFStream::EnumUnitTestFiles()
{   
#if defined( _WIN32 ) || defined( _WINDOWS )

   WIN32_FIND_DATA ffd;
   DWORD dwError = 0;

   // Find the first file in the directory.
   std::string testPath = m_testPath + "\\*";
   HANDLE hFind = FindFirstFile( testPath.c_str(), &ffd );

   if (INVALID_HANDLE_VALUE == hFind) {
      // ERROR
      return false;
   }

   do
   {
      // we just want normal file created by utgen
      if( !( ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) )
      {
         if( strstr( ffd.cFileName, ".h" ) ) {
            m_headerFiles.push_back( ffd.cFileName );
         }
         else {
            m_sourceFiles.push_back( ffd.cFileName );
         }
      }
   }
   while( FindNextFile(hFind, &ffd) != 0 );
   FindClose( hFind );

   dwError = GetLastError();
   if( dwError != ERROR_NO_MORE_FILES ) {
      // ERROR
      return false;
   }

#else

   struct stat fs;
   dirent* pFile = 0;

   DIR* pDir = opendir( m_testPath.c_str() );

   while( true )
   {
      pFile = readdir( pDir );
      if( !pFile ) break;

      stat( pFile->d_name, &fs );

      // we're only interested in regular files
      if( S_ISREG( fs.st_mode ) )
      {
         // create a list of header & source files
         if( strstr( pFile->d_name, ".h" ) ) {
            m_headerFiles.push_back( pFile->d_name );
         }
         else {
            m_sourceFiles.push_back( pFile->d_name );
         }
      }
   }

#endif
   return true;
}
