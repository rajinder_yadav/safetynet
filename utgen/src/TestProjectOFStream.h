/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

/** \brief 

   \file TestProjectOFStream.h

   \author Rajinder Yadav
   \date:  Jan 31, 2012

   web:   http://devmentor.org
   email: safetynet@devmentor.org

*/

#ifndef _CMAKE_OFSTREAM_H_
#define _CMAKE_OFSTREAM_H_

class ParseTokenValue;

/** \brief Required format of utgen inputs to generate a CMake test project

   ./utgen /project bin_name test_folder files ...

   /project - generate a CMake project file

   bin_name    - name of test binary

   test_folder - path to test folder

   files - one or more source files, space separated without path ( i.e person.cpp )

   NOTE: The source files are the source of the class we want to unit test
*/

class TestProjectOFStream
{
   const ParseTokenValue& m_tokenValue;
   std::ofstream m_ofsCMake;

   std::string m_testPath;        // path to unit test project folder
   std::string m_testBinaryName;  // unit test binary name

   std::vector<std::string> m_headerFiles;   // unit test header files
   std::vector<std::string> m_sourceFiles;   // unit test source files
   std::vector<std::string> m_subjectFiles;  // test class source files
  
public:
   TestProjectOFStream( const ParseTokenValue& tokens );

   bool EnumUnitTestFiles();
   void GenerateCMakeLists();
   void GenerateMain();
};

#endif // _CMAKE_OFSTREAM_H_
