/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

// Source: ParseTokenValue.h
// Author: Rajinder Yadav
// Date:   July 2, 2004
//
// web:   http://devmentor.org
// email: safetynet@devmentor.org

#ifndef _ParseTokenValue_h_
#define _ParseTokenValue_h_

enum GEN_MODE { UT_TESTCLASS, UT_CMAKE, UT_FACTORYCLASS };

class ParseTokenValue
{
   //---------------------
   // class static members
   //---------------------
   static const int TOKEN_COUNT = 10;
   static const std::string sm_strTokenSwitch[TOKEN_COUNT];
   

   //--------------
   // class members
   //--------------
   std::vector<std::string> m_TokenList;
   std::vector<std::string> m_UnitTestList;
   std::vector<std::string> m_FactoryList;
   std::vector<std::string> m_ProjectParams;

   std::string m_strClassName;
   std::string m_strAuthor;
   std::string m_strCopyright;
   std::string m_strSubject;
   std::string m_strOutputDir;

   GEN_MODE m_eGenerate;


   //---------
   // methods
   //---------

   // check if token list is empty, throw exception on empty list
   void ValidateTokenList( std::vector<std::string>::iterator& it ) const;

   inline bool IsSwitch(std::string s) const
   {
      for(int i=0; i < TOKEN_COUNT; ++i)
      {
         if( s == ParseTokenValue::sm_strTokenSwitch[i]) {
            return true;
         }
      }
      return false;
   }

   void ClassNameIs( std::vector<std::string>::iterator& it );
   void SubjectIs  ( std::vector<std::string>::iterator& it );
   void AuthorIs   ( std::vector<std::string>::iterator& it );
   void OutputDirIs( std::vector<std::string>::iterator& it );
   void CopyrightIs( std::vector<std::string>::iterator& it );
   void TestCaseIs ( std::vector<std::string>::iterator& it );

   void GenerateFactoryCode( std::vector<std::string>::iterator& it );
   void GenerateCMakeLists( std::vector<std::string>::iterator& it );

public:

   static void ShowUsage();

   void ValidateUTResults() const;

   ParseTokenValue(int argc, const char* argv[]);

   inline std::string ClassName() const { 
      return m_strClassName;
   }

   inline std::string Subject() const { 
      return m_strSubject;
   }

   inline std::string Author() const { 
      return m_strAuthor;
   }

   inline std::string OutputDir() const { 
      return m_strOutputDir;
   }
   
   inline std::string Copyright() const { 
      return m_strCopyright;
   }

   inline const int TestCaseSize() const {
      return static_cast<const int>( m_UnitTestList.size() );
   }

   inline std::string TestCase( int i ) const {
      return m_UnitTestList.at( i );
   }

   inline const int FactoryCaseSize() const {
      return static_cast<const int>( m_FactoryList.size() );
   }

   inline std::string FactoryCase( int i ) const {
      return m_FactoryList.at( i );
   }

   inline const int ProjectParamSize() const {
      return static_cast<const int>( m_ProjectParams.size() );
   }

   inline std::string ProjectParam( int i ) const {
      return m_ProjectParams.at( i );
   }

   inline GEN_MODE GenModeIs() const {
      return m_eGenerate;
   }
};

#endif // _ParseTokenValue_h_
