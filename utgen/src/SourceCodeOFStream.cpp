/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

// Source: SourceCodeOFStream.cpp
// Author: Rajinder Yadav
// Date:   July 2, 2004
//
// web:   http://devmentor.org
// email: safetynet@devmentor.org

#include "stdafx.h"
#include "ExceptionParameterInvalid.h"
#include "ParseTokenValue.h"
#include "SourceCodeOFStream.h"
#include <algorithm>

using std::cout;
using std::endl;

SourceCodeOFStream::SourceCodeOFStream(const ParseTokenValue& tokens) :
   m_tokenValue(tokens)
{
}

void SourceCodeOFStream::GenerateHeaderFile()
{
   std::string strFilename = m_tokenValue.ClassName() + ".h";
   std::transform( strFilename.begin(), 
                   strFilename.end(), 
                   strFilename.begin(), 
                   ::tolower );

   m_ofsCode.open( strFilename.c_str() );
   if( m_ofsCode.is_open() )
   {
      // format date
      time_t tm = {0};
      time( &tm );
      struct tm* pstm = localtime( &tm );
      
      char wzDate[40] = {0};
      strftime( wzDate, 40, "%B %d, %Y", pstm );

      m_ofsCode
      << "/** This unit test header file was generated using the utgen.exe utility" << endl
      << endl
      << "    \\file:   " << m_tokenValue.ClassName() << ".h" << endl
      << "    \\author: " << m_tokenValue.Author() << endl
      << "    \\date:   " << wzDate << endl
      << endl
      << "    copyright (c) " << m_tokenValue.Copyright() << " " << wzDate << endl
      << "*/"   << endl 
      << endl
      << "#ifndef _" << m_tokenValue.ClassName() << "_h_" << endl
      << "#define _" << m_tokenValue.ClassName() << "_h_" << endl
      << endl
      << "class " << m_tokenValue.Subject() << ";" << endl
      << endl
      << "class " << m_tokenValue.ClassName() << " : public UnitTestRunner" << endl
      << "{" << endl
      << "   " << m_tokenValue.Subject() << "* m_pSubject; // Class object that unit tests get applied too!!!" << endl << endl;


      for(int i=0; i < m_tokenValue.TestCaseSize(); ++i)
      {
         m_ofsCode << "   bool " << m_tokenValue.TestCase(i) << "_UT();" << endl;
      }

      m_ofsCode 
      << endl 
      << "public:" << endl 
      << endl 
      << "   REGISTER_TESTCLASS( " << m_tokenValue.ClassName() << " )" << endl
      << endl 
      << "   void Setup();"   << endl
      << "   void CleanUp();" << endl << endl
      << "   void RunTest()"  << endl
      << "   {" << endl
      << "      UNIT_TEST_START();" << endl << endl;

      for(int i=0; i < m_tokenValue.TestCaseSize(); ++i)
      {
         m_ofsCode << "         UNIT_TEST( " << m_tokenValue.TestCase(i) << "_UT );" <<endl;
      }

      m_ofsCode 
      << endl
      << "      UNIT_TEST_END();" << endl
      << "   }" << endl 
      << endl 
      << "};" << endl
      << "#endif // _" << m_tokenValue.ClassName() << "_h_" << endl;

      m_ofsCode.flush();
      m_ofsCode.close();
   }
}

void SourceCodeOFStream::GenerateSourceFile()
{
   std::string strFilename = m_tokenValue.ClassName() + ".cpp";
   std::transform( strFilename.begin(), 
                   strFilename.end(), 
                   strFilename.begin(), 
                   ::tolower );

   std::string subjectHeader   = m_tokenValue.Subject();
   std::string classNameHeader = m_tokenValue.ClassName();

   std::transform( subjectHeader.begin(), 
                   subjectHeader.end(), 
                   subjectHeader.begin(), 
                   ::tolower );

   std::transform( classNameHeader.begin(), 
                   classNameHeader.end(), 
                   classNameHeader.begin(), 
                   ::tolower );

   m_ofsCode.open( strFilename.c_str() );
   if( m_ofsCode.is_open() )
   {
      // format date
      time_t tm = {0};
      time( &tm );
      struct tm* pstm =localtime( &tm );
      
      char wzDate[40] = {0};
      strftime( wzDate, 40, "%B %d, %Y", pstm );

      m_ofsCode 
      << "/** This unit test header file was generated using the utgen.exe utility" << endl
      << endl
      << "    \\file:   " << m_tokenValue.ClassName() << ".h" << endl
      << "    \\author: " << m_tokenValue.Author() << endl
      << "    \\date:   " << wzDate << endl
      << endl
      << "    copyright (c) " << m_tokenValue.Copyright() << " " << wzDate << endl
      << "*/"   << endl 
      << endl
      << "#include <UnitTest.h>" << endl
      << "#include \"" << subjectHeader << ".h\"    // Linux is case-senstive" << endl
      << "#include \"" << classNameHeader << ".h\"" << endl
      << endl
      << "DECLARE_TESTRUNNER( " << m_tokenValue.ClassName() << " );" << endl
      << endl
      << "void " << m_tokenValue.ClassName() << "::" <<"Setup()" << endl
      << "{" << endl
      << "   // TODO: Add code to perform initialization before each unit test is executed." << endl
      << "   //       Allocate all the resources here to allow the unit test to execute.." << endl << endl
      << "   // TODO: use proper class constructor" << endl
      << "   m_pSubject = 0; // new " << m_tokenValue.Subject() << "(); // TODO: use proper class constructor" << endl
      << "}" << endl << endl 
      << "void " << m_tokenValue.ClassName() << "::" <<"CleanUp()" << endl
      << "{" << endl
      << "   // TODO: Add code to clean up after each unit test." << endl
      << "   //       Release all resource allocated in method SetUp()" << endl << endl
      << "   delete m_pSubject;" << endl
      << "   m_pSubject = 0;" << endl
      << "}" << endl << endl;


      for(int i=0; i < m_tokenValue.TestCaseSize(); ++i)
      {
         m_ofsCode 
         << "bool " << m_tokenValue.ClassName() << "::" << m_tokenValue.TestCase(i) << "_UT()" << endl
         << "{" << endl
         << "   bool bRet = false;" << endl
         << "   // TODO: add testing code here, return true for pass, false for fail.\n\n"
         << "   return bRet;" << endl
         << "}" << endl << endl;
      }

      m_ofsCode.flush();
      m_ofsCode.close();
   }
}
