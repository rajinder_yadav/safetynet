# ================================================================================
# Apache License Version 2.0
# 
# Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ================================================================================

cmake_minimum_required (VERSION 3.2)
project(utgen_test)

include_directories("${PROJECT_SOURCE_DIR}")
include_directories("${PROJECT_SOURCE_DIR}/..")
include_directories("$ENV{SAFETYNET_ROOT}/UnitTest/Include")

if(UT_NMAKE_BUILD)

   # Define this if you don't want to link with Qt SDK (will use WinSock)
   message("Setting up Windows NMake configurations and winsock2 linkage")
   SET(CMAKE_CXX_FLAGS "-D WINDOWS_BUILD")
   include_directories("$ENV{MSSDK_ROOT}/Include")
   link_directories("$ENV{MSSDK_ROOT}/Lib")
   set(UT_WINSOCK_LIB WS2_32.lib)

elseif(UT_LINUX_BUILD)

   # Define this, if you don't want to link with Qt SDK (will use tcp/ip sockets)
   message("Settings up Linux Make configuration and linkage")
   SET(CMAKE_CXX_FLAGS "-D LINUX_BUILD")

endif()

# Important! MinGW linker search path must be listed first
# Important! followed by NMake linker search path, otherwise MinGW builds will fail
link_directories("$ENV{SAFETYNET_ROOT}/UnitTest/LibMinGW" "$ENV{SAFETYNET_ROOT}/UnitTest/Lib")

#set(TestSubjectClasses ../ParseTokenValue.cpp ../ExceptionParameterInvalid.cpp)

set(SafetyNetTest_Header ParseTokenValue_UnitTest.h)

set(SafetyNetTest_Source TestMain.cpp ParseTokenValue_UnitTest.cpp ${TestSubjectClasses})

add_executable(utgen_test ${UIS_HDRS} ${SafetyNetTest_Source} )
target_link_libraries(utgen_test UnitTest ${UT_WINSOCK_LIB})
