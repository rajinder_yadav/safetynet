/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

/** This unit test header file was generated using the utgen.exe utility

    \file:   ParseTokenValue_UnitTest.h
    \author: 
    \date:   February 11, 2012
*/

#ifndef _ParseTokenValue_UnitTest_h_
#define _ParseTokenValue_UnitTest_h_

class ParseTokenValue;

class ParseTokenValue_UnitTest : public UnitTestRunner
{
   ParseTokenValue* m_pSubject; // Class object that unit tests get applied too!!!

   bool InvalidParam_UT1();
   bool InvalidParam_UT2();
   bool InvalidParam_UT3();

   bool ValidParam_UT1();

   bool ClassName_UT1();
   bool ClassName_UT2();
   bool ClassName_UT3();

   bool Subject_UT1();
   bool Subject_UT2();

   bool Author_UT1();
   bool Author_UT2();
   bool Author_UT3();

   bool OutputDir_UT();

   bool Copyright_UT1();
   bool Copyright_UT2();
   bool Copyright_UT3();

   bool TestCaseSize_UT1();
   bool TestCaseSize_UT2();

   bool TestCase_UT1();
   bool TestCase_UT2();
   bool TestCase_UT3();
   bool TestCase_UT4();

   bool ProjectPrameSize_UT();

   bool ProjectParam_UT1();
   bool ProjectParam_UT2();
   bool ProjectParam_UT3();
   bool ProjectParam_UT4();

   bool GetModeIs_UT1();
   bool GetModeIs_UT2();
   bool GetModeIs_UT3();

public:

   REGISTER_TESTCLASS( ParseTokenValue_UnitTest )

   void Setup();
   void CleanUp();

   void RunTest()
   {
      UNIT_TEST_START();
         UNIT_TEST( InvalidParam_UT1 );
         UNIT_TEST( InvalidParam_UT2 );
         UNIT_TEST( InvalidParam_UT3 );

         UNIT_TEST( ValidParam_UT1 );

         UNIT_TEST( ClassName_UT1 );
         UNIT_TEST( ClassName_UT2 );
         UNIT_TEST( ClassName_UT3 );

         UNIT_TEST( Subject_UT1 );
         UNIT_TEST( Subject_UT2 );

         UNIT_TEST( Author_UT1 );
         UNIT_TEST( Author_UT2 );
         UNIT_TEST( Author_UT3 );

         UNIT_TEST( OutputDir_UT );

         UNIT_TEST( Copyright_UT1 );
         UNIT_TEST( Copyright_UT2 );
         UNIT_TEST( Copyright_UT3 );

         UNIT_TEST( TestCaseSize_UT1 );
         UNIT_TEST( TestCaseSize_UT2 );

         UNIT_TEST( TestCase_UT1 );
         UNIT_TEST( TestCase_UT2 );
         UNIT_TEST( TestCase_UT3 );
         UNIT_TEST( TestCase_UT4 );

         UNIT_TEST( ProjectPrameSize_UT );

         UNIT_TEST( ProjectParam_UT1 );
         UNIT_TEST( ProjectParam_UT2 );
         UNIT_TEST( ProjectParam_UT3 );
         UNIT_TEST( ProjectParam_UT4 );

         UNIT_TEST( GetModeIs_UT1 );
         UNIT_TEST( GetModeIs_UT2 );
         UNIT_TEST( GetModeIs_UT3 );

      UNIT_TEST_END();
   }

};
#endif // _ParseTokenValue_UnitTest_h_
