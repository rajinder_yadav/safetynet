/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

/** This unit test header file was generated using the utgen.exe utility

    \file:   ParseTokenValue_UnitTest.h
    \author: 
    \date:   February 11, 2012
*/

#include <UnitTest.h>
#include "ParseTokenValue.h"    // Linux is case-senstive
#include "ParseTokenValue_UnitTest.h"
#include "ExceptionParameterInvalid.h"
#include <stdexcept>

DECLARE_TESTRUNNER( ParseTokenValue_UnitTest );

void ParseTokenValue_UnitTest::Setup()
{
   // TODO: Add code to perform initialization before each unit test is executed.
   //       Allocate all the resources here to allow the unit test to execute..

   //m_pSubject = new ParseTokenValue(); // TODO: use proper class constructor
}

void ParseTokenValue_UnitTest::CleanUp()
{
   // TODO: Add code to clean up after each unit test.
   //       Release all resource allocated in method SetUp()

   delete m_pSubject;
   m_pSubject = 0;
}

bool ParseTokenValue_UnitTest::InvalidParam_UT1()
{
   bool bRet = false;
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "/t", "open", "close", "save" };
   SN_TEST_EXCEPTION( m_pSubject = new ParseTokenValue( 7, argv ), 
                      ExceptionParameterInvalid );
   return bRet;
}

bool ParseTokenValue_UnitTest::InvalidParam_UT2()
{
   bool bRet = false;
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/s" };
   SN_TEST_EXCEPTION( m_pSubject = new ParseTokenValue( 2, argv ) , 
                      ExceptionParameterInvalid );
   return bRet;
}

bool ParseTokenValue_UnitTest::InvalidParam_UT3()
{
   bool bRet = false;
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "NetTest", "open", "close", "save" };
   SN_TEST_EXCEPTION( m_pSubject = new ParseTokenValue( 6, argv ), 
                      ExceptionParameterInvalid );
   return bRet;
}

bool ParseTokenValue_UnitTest::ValidParam_UT1()
{
   bool bRet = true;
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/s", "NetTest", "/a", "Yadav",
   "/c", "devmentor.org", "/t" ,"open", "close", "save" };
   m_pSubject = new ParseTokenValue( 11, argv );
   SN_VALIDATE_PASS( m_pSubject->Subject() == "NetTest" );
   SN_VALIDATE_PASS( m_pSubject->ClassName() == "NetTest_UnitTest" );
   SN_VALIDATE_PASS( m_pSubject->Author() == "Yadav" );
   SN_VALIDATE_PASS( m_pSubject->Copyright() == "devmentor.org" );
   SN_VALIDATE_PASS( m_pSubject->TestCaseSize() == 3 );
   SN_VALIDATE_PASS( m_pSubject->TestCase( 0 ) == "open" );
   SN_VALIDATE_PASS( m_pSubject->TestCase( 1 ) == "close" );
   SN_VALIDATE_PASS( m_pSubject->TestCase( 2 ) == "save" );
   return bRet;
}

bool ParseTokenValue_UnitTest::ClassName_UT1()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "subject" };
   m_pSubject = new ParseTokenValue( 3, argv );
   SN_TEST_PASS( m_pSubject->ClassName() == "subject" );
}

bool ParseTokenValue_UnitTest::ClassName_UT2()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/s", "subject2" };
   m_pSubject = new ParseTokenValue( 3, argv );
   SN_TEST_PASS( m_pSubject->ClassName() == "subject2_UnitTest" );
}

bool ParseTokenValue_UnitTest::ClassName_UT3()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/project", "subject2", "." };
   m_pSubject = new ParseTokenValue( 4, argv );
   SN_TEST_PASS( m_pSubject->ClassName().empty() );
}

bool ParseTokenValue_UnitTest::Subject_UT1()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/s", "subject2" };
   m_pSubject = new ParseTokenValue( 3, argv );
   SN_TEST_PASS( m_pSubject->Subject() == "subject2" );
}

bool ParseTokenValue_UnitTest::Subject_UT2()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "people" };
   m_pSubject = new ParseTokenValue( 3, argv );
   SN_TEST_PASS( m_pSubject->Subject().empty() );
}

bool ParseTokenValue_UnitTest::Author_UT1()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "people", "/a", "Rajinder", "Yadav" };
   m_pSubject = new ParseTokenValue( 6, argv );
   SN_TEST_PASS( m_pSubject->Author() == "Rajinder Yadav" );
}

bool ParseTokenValue_UnitTest::Author_UT2()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "people", "/a", "Rajinder" };
   m_pSubject = new ParseTokenValue( 5, argv );
   SN_TEST_PASS( m_pSubject->Author() == "Rajinder" );
}

bool ParseTokenValue_UnitTest::Author_UT3()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "people", "/a", "Rajinder", "Yadav", "/c", "holder" };
   m_pSubject = new ParseTokenValue( 8, argv );
   SN_TEST_PASS( m_pSubject->Author() == "Rajinder Yadav" );
}

bool ParseTokenValue_UnitTest::OutputDir_UT()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "people", "/a", "Rajinder" };
   m_pSubject = new ParseTokenValue( 5, argv );
   SN_TEST_PASS( m_pSubject->OutputDir() == ".\\" || m_pSubject->OutputDir() == "./" );
}

bool ParseTokenValue_UnitTest::Copyright_UT1()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "people", "/a", "Rajinder", "Yadav" };
   m_pSubject = new ParseTokenValue( 6, argv );
   SN_TEST_PASS( m_pSubject->Copyright().empty() );
}


bool ParseTokenValue_UnitTest::Copyright_UT2()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "people", "/a", "Rajinder", "Yadav", "/c", "holder" };
   m_pSubject = new ParseTokenValue( 8, argv );
   SN_TEST_PASS( m_pSubject->Copyright() == "holder" );
}

bool ParseTokenValue_UnitTest::Copyright_UT3()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "people", "/c", "Rajinder", "Yadav", "/a", "holder" };
   m_pSubject = new ParseTokenValue( 8, argv );
   SN_TEST_PASS( m_pSubject->Copyright() == "Rajinder Yadav" );
}

bool ParseTokenValue_UnitTest::TestCaseSize_UT1()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "test", "/t", "open", "close", "save" };
   m_pSubject = new ParseTokenValue( 7, argv );
   SN_TEST_PASS( m_pSubject->TestCaseSize() == 3 );
}

bool ParseTokenValue_UnitTest::TestCaseSize_UT2()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "test" };
   m_pSubject = new ParseTokenValue( 3, argv );
   SN_TEST_PASS( m_pSubject->TestCaseSize() == 0 );
}

bool ParseTokenValue_UnitTest::TestCase_UT1()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "test", "/t", "open", "close", "save" };
   m_pSubject = new ParseTokenValue( 7, argv );
   SN_TEST_PASS( m_pSubject->TestCase( 0 ) == "open" );
}

bool ParseTokenValue_UnitTest::TestCase_UT2()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "test", "/t", "open", "close", "save" };
   m_pSubject = new ParseTokenValue( 7, argv );
   SN_TEST_PASS( m_pSubject->TestCase( 1 ) == "close" );
}

bool ParseTokenValue_UnitTest::TestCase_UT3()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "test", "/t", "open", "close", "save" };
   m_pSubject = new ParseTokenValue( 7, argv );
   SN_TEST_PASS( m_pSubject->TestCase( 2 ) == "save" );
}

bool ParseTokenValue_UnitTest::TestCase_UT4()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "test", "/t", "open", "close", "save" };
   m_pSubject = new ParseTokenValue( 7, argv );
   SN_TEST_EXCEPTION( m_pSubject->TestCase( 3 ), std::out_of_range );
}

bool ParseTokenValue_UnitTest::ProjectPrameSize_UT()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/project", "my_test", ".", "open", "close", "save" };
   m_pSubject = new ParseTokenValue( 7, argv );
   SN_TEST_PASS( m_pSubject->ProjectParamSize() == 5 );
}

bool ParseTokenValue_UnitTest::ProjectParam_UT1()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/project", "my_test" };
   SN_TEST_EXCEPTION( m_pSubject = new ParseTokenValue( 3, argv ),
                      ExceptionParameterInvalid );
}

bool ParseTokenValue_UnitTest::ProjectParam_UT2()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/project" };
   SN_TEST_EXCEPTION( m_pSubject = new ParseTokenValue( 2, argv ),
                      ExceptionParameterInvalid );
}

bool ParseTokenValue_UnitTest::ProjectParam_UT3()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/project", "my_test", "." };
   m_pSubject = new ParseTokenValue( 7, argv );
   SN_TEST_PASS( m_pSubject->ProjectParam( 1 ) == "." );
}

bool ParseTokenValue_UnitTest::ProjectParam_UT4()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/project", "my_test", ".", "file.cpp", "db.cpp", "net.cpp" };
   m_pSubject = new ParseTokenValue( 7, argv );
   SN_TEST_PASS( m_pSubject->ProjectParam( 3 ) == "db.cpp" );
}

bool ParseTokenValue_UnitTest::GetModeIs_UT1()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/project", "my_test", ".", "file.cpp", "db.cpp", "net.cpp" };
   m_pSubject = new ParseTokenValue( 7, argv );
   SN_TEST_PASS( m_pSubject->GenModeIs() == UT_CMAKE );
}

bool ParseTokenValue_UnitTest::GetModeIs_UT2()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/s", "my_test", "/t", "file.cpp", "db.cpp", "net.cpp" };
   m_pSubject = new ParseTokenValue( 7, argv );
   SN_TEST_PASS( m_pSubject->GenModeIs() == UT_TESTCLASS );
}


bool ParseTokenValue_UnitTest::GetModeIs_UT3()
{
   // TODO: add testing code here, return true for pass, false for fail.
   const char* argv[] = { "utgen", "/n", "my_test", "/t", "file.cpp", "db.cpp", "net.cpp" };
   m_pSubject = new ParseTokenValue( 7, argv );
   SN_TEST_PASS( m_pSubject->GenModeIs() == UT_TESTCLASS );
}
