/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

/** \brief This file define the macros used in building a unit test class

   \file UnitTestDefs.h

   \author Rajinder Yadav
   \date   June 30, 2004

   web:   http://safetynet.devmentor.org
   email: safetynet@devmentor.org
*/


#ifndef _UnitTestDefs_h_
#define _UnitTestDefs_h_

//-------------------------------------------------------------
// Following macros are to be use only from within 
// class UnitTestRunner or it's derivities where possible
//-------------------------------------------------------------


//-------------------------------------------------------------
// UNIT_TEST macro used by a test class to envoke 
// the named test method that returns a boolean value
// 
// return vaues:
// true  - signifies the test case passed
// false - signifies the test case failed
//-------------------------------------------------------------   
#define UNIT_TEST( _TC )                       \
   m_fnList.push_back( &__utThisClass::_TC );  \
   m_strTestName.push_back( #_TC );                                          



//----------------------------------------------------------------
// UNIT_TEST_START()
//
// set the test file location then notify all unit test observer
// that the we are going to start with all the unit tests
//----------------------------------------------------------------
#define UNIT_TEST_START()              \
   m_fnList.clear();                   \
   m_strTestName.clear();



//-------------------------------------------------------------
// UNIT_TEST_END()
//
// notify all the unit test observer the we have completed
// all of the unit tests
//-------------------------------------------------------------
#define UNIT_TEST_END()                   \
   if( m_bPrepareTestCases == false ) {   \
      ExecuteTest();                      \
   }



//-------------------------------------------------------------
// REGISTER_TESTCLASS( _CLSNAME )
//
// use this macro in the test class definition 
// to declare a default ctor that will call the base
// class ctor to save the class name and register
// the test class with the test framework
//-------------------------------------------------------------
#define REGISTER_TESTCLASS( _CLSNAME )                      \
   typedef class _CLSNAME __utThisClass;                    \
   std::vector<bool (__utThisClass::*)(void)> m_fnList;     \
   _CLSNAME() : UnitTestRunner( #_CLSNAME )                 \
   {                                                        \
      UnitTestAssembly::GetInstance().RegisterTest(*this);  \
   }                                                        \
   bool ExecuteTestCase( size_t i ) {                       \
      return (this->*m_fnList[i])();                        \
   }                                                        \
   size_t TestCaseCount() {                                 \
      return m_fnList.size();                               \
   }



//-------------------------------------------------------------
// DECLARE_TESTRUNNER( _CLSNAME )
//
// use this macro in the source file of a test class to declare
// a global object to start the process of registering 
// the test runner class
//-------------------------------------------------------------
#define DECLARE_TESTRUNNER( _CLSNAME ) \
   _CLSNAME obj_##_CLSNAME


//-------------------------------------------------------------
// use this macro inside the main test harnes to register the
// observers so it will be notify of test case result
//-------------------------------------------------------------
#define DECLARE_OBSERVER( _OBSERVER ) \
   UnitTestAssembly::GetInstance().RegisterObserver( _OBSERVER )

//-------------------------------------------------------------
// this macro should be used to start the unit test 
// it will created observers for logging and gui monitoring
//-------------------------------------------------------------
#define START_UNIT_TEST( _LOGFILE )          \
   StreamLogger _fileLogger( _LOGFILE );     \
   DECLARE_OBSERVER( _fileLogger );          \
   MonitorLogger _monitorLogger;             \
   DECLARE_OBSERVER( _monitorLogger );       \
   UnitTestManager::Start();

//-------------------------------------------------------------
// test exception by executing code passed in as _STATEMENT
// _EXCEPTION specify the exception type to be caught
// if an exception is caught, a true is returned
//-------------------------------------------------------------
#define SN_TEST_EXCEPTION( _STATEMENT, _EXCEPTION )  \
try {                                                \
   ( _STATEMENT );                                   \
} catch( _EXCEPTION& e ) {                           \
   return true;                                      \
}

//-------------------------------------------------------------
// execute the code passed as _STATEMENT 
// return true is it passed
//-------------------------------------------------------------
#define SN_TEST_PASS( _STATEMENT )  \
if( _STATEMENT ) return true;       \
return false;

//-------------------------------------------------------------
// execute the code passed as _STATEMENT
// return true is it failed
//-------------------------------------------------------------
#define SN_TEST_FAIL( _STATEMENT )  \
if( !(_STATEMENT) ) return true;    \
return false;

//-------------------------------------------------------------
// execute code passed as _STATEMENT
// return false if validation fails, otherwise continue
//-------------------------------------------------------------
#define SN_VALIDATE_PASS( _STATEMENT ) \
if( !(_STATEMENT) ) return false;

//-------------------------------------------------------------
// execute code passed as _STATEMENT
// return false if validation passes, otherwise continue
//-------------------------------------------------------------
#define SN_VALIDATE_FAIL( _STATEMENT ) \
if( _STATEMENT ) return false;

#endif // _UnitTestDefs_h_
