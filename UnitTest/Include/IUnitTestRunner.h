/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

/** \brief Unit test class must implement this interface

   \file IUnitTestRunner.h

   \author Rajinder Yadav
   \date   June 30, 2004

   web:   http://safetynet.devmentor.org
   email: safetynet@devmentor.org
*/

#ifndef _IUnitTestRunner_h_
#define _IUnitTestRunner_h_

/** \brief unit test interface

   All test case classes are required to implement this interface
   called by UnitTestAssembly during each unit test case run
*/
struct IUnitTestRunner
{
   /// Called before each unit test starts
   virtual void Setup()   = 0;

   /// Called after each unit test has completed
   virtual void CleanUp() = 0;

   /// Perform ALL the unit tests here, called ONCE for each unit test class
   virtual void RunTest() = 0;
};

#endif // _IUnitTestRunner_h_
