/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

/** \brief Main include header file,

   \file UnitTest.h

   \author Rajinder Yadav
   \date   July 5, 2004

   web:   http://safetynet.devmentor.org
   email: safetynet@devmentor.org
*/



#ifndef _UnitTest_h_
#define _UnitTest_h_

#if defined( _WINDOWS ) || defined( WINDOWS_BUILD )
   // Exclude rarely-used stuff from Windows headers
   #define WIN32_LEAN_AND_MEAN
   #include <windows.h>

	#ifdef UTCORE_EXPORTS
		#define UTCORE_API __declspec(dllexport)
	#else
		#define UTCORE_API __declspec(dllimport)
	#endif
#else
	#define UTCORE_API
#endif

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <map>

#include <UnitTestDefs.h>
#include <IUnitTestRunner.h>
#include <UnitTestAssembly.h>
#include <UnitTestRunner.h>
#include <IUnitTestObserver.h>
#include <StreamLogger.h>
#include <ConsoleLogger.h>
#include <MonitorLogger.h>


#endif // _UnitTest_h_
