/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

/** \brief Unit test observer interface

   \file IUnitTestObserver.h

   \author Rajinder Yadav
   \date   July 3, 2004

   web:   http://safetynet.devmentor.org
   email: safetynet@devmentor.org
*/

#ifndef _IUnitTestObserver_h_
#define _IUnitTestObserver_h_

/** \brief observer interface

   A class must implement this interface if it wants to be notified of 
   unit test results: { pass, fail, exception }

   The observer class needs to register itself with a UnitTestRunner
   event source before notification are sent through this interface
*/
struct IUnitTestObserver
{
   /// Pre-unit test execution
   virtual void UnitTestStart( IUnitTestRunner& subject ) = 0;

   /// Post-unit test execution
   virtual void UnitTestEnd( IUnitTestRunner& subject )   = 0;

   /// Failure notification
   virtual void Failed( IUnitTestRunner& subject )        = 0;

   /// Success notification
   virtual void Passed( IUnitTestRunner& subject )        = 0;

   /// C++ exception notification
   virtual void Exception( IUnitTestRunner& subject )     = 0;
};

#endif // _IUnitTestObserver_h_
