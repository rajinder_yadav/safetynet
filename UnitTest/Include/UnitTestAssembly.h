/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

/** \brief UnitTestAssembly execute all test cases and hold the result of each

   \file UnitTestAssembly.h

   \author Rajinder Yadav
   \date   June 30, 2004

   web:   http://safetynet.devmentor.org
   email: safetynet@devmentor.org
*/

#ifndef _UnitTestAssembly_
#define _UnitTestAssembly_

/// warning C4251: 'UnitTestAssembly::subject' : class 'std::vector<_Ty>' needs to have dll-interface to be used by clients of class 'UnitTestAssembly'
#pragma warning(disable:4251)

// forward declrations
struct IUnitTestRunner;
struct IUnitTestObserver;

/** \brief UnitTestAssembly is responsible for running all test cases

   It also is the container and notifier of test results to
   attached observers. This class is a singleton pattern, use its
   factory method to create or get an instance.
*/
class UTCORE_API UnitTestAssembly
{
   static UnitTestAssembly* sm_UnitTestAssembly;

   std::map<std::string, std::string> m_mapUTInput;
   typedef std::pair<std::string, std::string> pair_utInput;

   /// registered test case class
   std::vector<IUnitTestRunner*> m_Subject;
   
#ifdef _WINDOWS
   LPTOP_LEVEL_EXCEPTION_FILTER m_SEHFilter;
#endif

   // unit test execution pointers
   int m_iSubject;
   int m_iTestCase;

   UnitTestAssembly();
   ~UnitTestAssembly();

public:
   struct UTCORE_API SubjectSelectNode
   {
      bool bRunSubject;
      std::vector<bool> TestCaseList;
   };
   typedef std::vector<SubjectSelectNode> tSubjectSelectNodeList;

   tSubjectSelectNodeList m_stlSubjectSelectionList;

   //------------------------------
   // static class factory methods
   //------------------------------
   static UnitTestAssembly& GetInstance()
   {
      if( UnitTestAssembly::sm_UnitTestAssembly == 0 )
      {
         UnitTestAssembly::sm_UnitTestAssembly = new UnitTestAssembly;
      }
      return *UnitTestAssembly::sm_UnitTestAssembly;
   }

   static void ReleaseInstance()
   {
      delete UnitTestAssembly::sm_UnitTestAssembly;
      UnitTestAssembly::sm_UnitTestAssembly = 0;
   }

   //---------------------------------------
   // selection subject (test case) methods
   //---------------------------------------
   inline void AddSelectedSubject( SubjectSelectNode& rNode ) {
      m_stlSubjectSelectionList.push_back( rNode );
   }

   int SelectedTestCaseCount()
   {
      size_t iCount = 0;
      const size_t nSubjects = m_stlSubjectSelectionList.size();
      for( size_t i=0; i < nSubjects; ++i )
      {
         if( m_stlSubjectSelectionList[i].bRunSubject )
         {
            ++iCount;
            const size_t nTestCases = m_stlSubjectSelectionList[i].TestCaseList.size();

            for( size_t j=0; j < nTestCases; ++j )
            {
               if( m_stlSubjectSelectionList[i].TestCaseList[j] ) {
                  ++iCount;
               }
            } // for
         }
      } // for
      return (int)iCount;
   }

   void SubjectSelectIs( bool bCheck, int iSubject ) {
      m_stlSubjectSelectionList[iSubject].bRunSubject = bCheck;
   }

   bool SubjectSelectIs( int iSubject ) {
      m_iSubject = iSubject;
      return m_stlSubjectSelectionList[iSubject].bRunSubject;
   }

   void SubjectTestCaseSelectIs( bool bCheck, int iSubject, int iTestCase ) {
      m_stlSubjectSelectionList[iSubject].TestCaseList[iTestCase] = bCheck;
   }

   bool SubjectTestCaseSelectIs( int iTestCase ) {
      m_iTestCase = iTestCase;
      return m_stlSubjectSelectionList[m_iSubject].TestCaseList[iTestCase];
   }

   //------------------------------
   // public methods
   //------------------------------
   inline char* GetUnitTestInput( char* wzField )
   {
      char* pwzValue = '\0';
      std::map<std::string, std::string>::iterator it;
      it = m_mapUTInput.find( wzField );
      if( it != m_mapUTInput.end() ) {
         pwzValue = const_cast<char*>( it->second.c_str() );
      }
      return pwzValue;
   }

   void ResetTestResults();

   int GetTestList( std::vector<IUnitTestRunner*>& subjectList );

   // register observers with each contained subjects in assembly
   void RegisterObserver(IUnitTestObserver& observer);

   // register subject into assembly
   void RegisterTest( IUnitTestRunner& subject );
   void RemoveTest( IUnitTestRunner& subject );

   inline void ResetTestList() {
      m_Subject.clear();
   }

   void Run();
};

#ifdef _WINDOWS
UTCORE_API LONG WINAPI ryExceptionHandler(struct _EXCEPTION_POINTERS *ExceptionInfo);
#endif

//------------------------------------------------
// helper class, encapsulates factory operations
//------------------------------------------------
class UTCORE_API UnitTestManager
{
public:
   static void Start()
   {
      // create test harness and begin the unit test
      UnitTestAssembly::GetInstance().Run();

      // free singleton unit test framework
      UnitTestAssembly::ReleaseInstance();
   }
};
#endif // _UnitTestAssembly_
