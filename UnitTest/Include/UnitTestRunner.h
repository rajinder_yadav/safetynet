/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

/** \brief UnitTestRunner is the main test engine

   \description This class is responsible adding test cases and maintaining
   a list on observer that are to be notified of each test result
   
   \file UnitTestRunner.h

   \author Rajinder Yadav
   \date   June 30, 2004

   web:   http://safetynet.devmentor.org
   email: safetynet@devmentor.org
*/

#ifndef _UnitTestRunner_h_
#define _UnitTestRunner_h_

#pragma warning(disable:4251) // warning C4251: 'UnitTestRunner::m_ObserverList' : class 'std::vector<_Ty>' needs to have dll-interface to be used by clients of class 'UnitTestRunner'

// forward declaration
struct IUnitTestRunner;
struct IUnitTestObserver;

extern bool g_bSEHException;

class UTCORE_API UnitTestRunner : public IUnitTestRunner
{
protected:
   friend class UnitTestRunner_UnitTest;

   // optimized in most likely, most occuring order
   enum TESTCASE_EVENT
   {
      TC_EVENT_PASSED,
      TC_EVENT_START,
      TC_EVENT_END,
      TC_EVENT_FAILED,
      TC_EVENT_EXCEPTION
   };

   std::vector<IUnitTestObserver*> m_ObserverList;

   std::string m_strTestClass;   // unit test class name
   std::string m_strTestCase;    // unit test case name, currently executing

   std::stringstream m_strMessageStream;

   long m_nTestCases;   // count, total running test cases
   long m_nFailed;      // count, failed test cases
   long m_nExceptions;  // count, unhandeled exceptions

   bool m_bPrepareTestCases;  // when true, do not execute test cases

   //------------------------------
   // (internal) framework methods
   //------------------------------
   inline void IncTestCount()   { ++m_nTestCases;   }
   inline void IncFailed()      { ++m_nFailed;      }
   inline void IncException()   { ++m_nExceptions;  }

public:
   std::vector<std::string> m_strTestName;

   UnitTestRunner(const char* const name);
   virtual ~UnitTestRunner();

   //---------------
   // helper APIs
   //---------------
   virtual bool ExecuteTestCase( size_t i ) = 0;
   virtual size_t TestCaseCount()           = 0;

   void ExecuteTest();

   void PrepareTestCases() {
      m_bPrepareTestCases = true;
      RunTest();
      m_bPrepareTestCases = false;
   }

   //---------------------------
   // interface IUnitTestRunner
   //---------------------------
   virtual void Setup() { }
   virtual void CleanUp() { }
   virtual void RunTest() = 0;

   //--------------
   // general APIs
   //--------------
   inline long TestCountIs() const      { return m_nTestCases;  }
   inline long FailedCountIs() const    { return m_nFailed;     }
   inline long ExceptionCountIs() const { return m_nExceptions; }

   inline std::string ClassNameIs() const { return m_strTestClass; }

   inline void TestCaseIs(std::string name) { m_strTestCase = name; }
   inline std::string TestCaseIs() const { return m_strTestCase; }

   inline void ClearTestMsg() { m_strMessageStream.rdbuf()->str( "" ); }
   inline void AppendTestMsg(std::string msg) { m_strMessageStream << msg; }
   inline std::string TestMsgIs() const { return m_strMessageStream.str(); }

   inline void AddObserver(IUnitTestObserver& observer)
   {
      m_ObserverList.push_back(&observer);
   }

   inline void RemoveObserver( IUnitTestObserver& observer )
   {
      std::vector<IUnitTestObserver*>::iterator it =
      std::remove( m_ObserverList.begin(), m_ObserverList.end(), &observer );
      if( it != m_ObserverList.end() ) {
         m_ObserverList.erase( it );
      }
   }

   inline char* UnitTestInputIs( char* pwzField ) const
   {
      return UnitTestAssembly::GetInstance().GetUnitTestInput( pwzField );
   }

   inline void Reset()
   {
      m_nTestCases  = 0;
      m_nFailed     = 0;
      m_nExceptions = 0;
      ClearTestMsg();
      m_bPrepareTestCases = false;
   }

   void NofityObserver(TESTCASE_EVENT eResult);
};

#endif // _UnitTestRunner_h_
