/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

/** \brief StreamLogger logs unit test results to a file

   \file StreamLogger.h
   
   \author Rajinder Yadav
   \date   July 4, 2004

   web:   http://safetynet.devmentor.org
   email: safetynet@devmentor.org
*/

#ifndef _StreamLogger_h_
#define _StreamLogger_h_

class UTCORE_API StreamLogger : public IUnitTestObserver
{
   unsigned long m_nTestRun;
   unsigned long m_nFailed;
   unsigned long m_nExceptions;

   std::ofstream m_LogFile;
   std::string m_strFilename;
   std::stringstream m_ssErrors;

public:

   StreamLogger();
   StreamLogger( std::string strFilename );
   ~StreamLogger();

   void Open( std::string strFilename );
   void Close();

   // Interface IUnitTestObserver
   void UnitTestStart( IUnitTestRunner& subject );
   void UnitTestEnd( IUnitTestRunner& subject );

   void Failed( IUnitTestRunner& subject );
   void Passed( IUnitTestRunner& subject );
   void Exception( IUnitTestRunner& subject );
};

#endif // _StreamLogger_h_
