/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

/** \brief Log unit test result to the console window

   \file MonitorLogger.cpp

   \author Rajinder Yadav
   \date   July 5, 2004

   web:   http://safetynet.devmentor.org
   email: safetynet@devmentor.org
*/

#define UTCORE_API

#include <UnitTest.h>
#include "MonitorLogger.h"
#include "asio.hpp"
#include <iostream>
#include <ctime>
#include <cstring>
#include <sstream>

using std::cout;
using std::endl;
using asio::ip::udp;

#pragma warning ( disable : 4996 )

#ifdef _sleep
   #undef _sleep
#endif

// note _WIN32 is defined by MinGW
#if defined ( _WINDOWS ) || defined( _WIN32)
   // Windows sleep is in milli-sec
   #define _sleep(v)
#else
   #include <unistd.h>
   // Linux sleep is in micro-sec
   #define _sleep(v) usleep(v)
#endif

MonitorLogger::MonitorLogger()
{
   m_nTestRun      = 0;
   m_nFailed       = 0;
   m_nExceptions   = 0;
}

MonitorLogger::~MonitorLogger()
{
   if( m_ssErrors.rdbuf()->in_avail() > 0 )
   {
      m_ssResults
      << "<font color='blue'><b>Summary of Failures</b><br/>"
      << m_ssErrors.str() << "<br/>"
      << endl;
   }

   m_ssResults
   << "<font color='blue'><b>Overall Summary</b><br/>"
   << "Total Run: "              << m_nTestRun     << "<br/>"
   << "Total Failed: "           << m_nFailed      << "<br/>"
   << "Total Exceptions: "       << m_nExceptions  << "<br/>"
   << "</font>"
   << endl;

   postTestResult();
}

void MonitorLogger::UnitTestStart(IUnitTestRunner& subject)
{
   m_ssResults
   << "<font color='blue'>Test Subject: "
   << static_cast<UnitTestRunner&>(subject).ClassNameIs()
   << "<p/>Unit Test Started</font><br/>"
   << endl;
}

void MonitorLogger::Failed(IUnitTestRunner& subject)
{
   m_ssResults
   << "<font color='red'>" << static_cast<UnitTestRunner&>(subject).TestMsgIs()
   << "</font><br/>"
   << endl;

   m_ssErrors
   << "<font color='red'>" << static_cast<UnitTestRunner&>(subject).TestMsgIs()
   << "</font><br/>"
   << endl;
}


void MonitorLogger::Passed(IUnitTestRunner& subject)
{
   m_ssResults
   << "<font color='green'>" << static_cast<UnitTestRunner&>(subject).TestMsgIs()
   << "</font><br/>"
   << endl;
}

void MonitorLogger::Exception(IUnitTestRunner& subject)
{
   m_ssResults
   << "<font color='purple'>" << static_cast<UnitTestRunner&>(subject).TestMsgIs()
   << "</font><br/>"
   << endl;

   m_ssErrors
   << "<font color='purple'>" << static_cast<UnitTestRunner&>(subject).TestMsgIs()
   << "</font><br/>"
   << endl;
}

void MonitorLogger::UnitTestEnd(IUnitTestRunner& subject)
{
   m_ssResults
   << "<font color='blue'>Unit Test complete.<br/><br/><b>Test Summary: Tests("
   << static_cast<UnitTestRunner&>(subject).TestCountIs() << ")"
   << " Fails("
   << static_cast<UnitTestRunner&>(subject).FailedCountIs() << ")"
   << " Exceptions("
   << static_cast<UnitTestRunner&>(subject).ExceptionCountIs() << ")"
   << "</b></font><br/><br/>"
   << endl;

   m_nTestRun    += static_cast<UnitTestRunner&>(subject).TestCountIs();
   m_nFailed     += static_cast<UnitTestRunner&>(subject).FailedCountIs();
   m_nExceptions += static_cast<UnitTestRunner&>(subject).ExceptionCountIs();
}

void MonitorLogger::postTestResult()
{
   const int BUF_SIZE = 1024;
   char buf[BUF_SIZE];
   memset( buf, 0, BUF_SIZE );

   asio::io_service io_service;
   udp::endpoint ep( udp::v4(), 61371 );
   udp::socket socket( io_service );
   socket.open( udp::v4() );
   
   while( !m_ssResults.eof() )
   {
      m_ssResults.getline( buf, BUF_SIZE, '\n' );
      socket.send_to( asio::buffer( buf ), ep );
      _sleep( 250 );
   }
}
