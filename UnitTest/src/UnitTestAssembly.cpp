/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

/** \brief UnitTestAssembly execute all test cases and hold the result of each

   \file UnitTestAssembly.cpp

   \author Rajinder Yadav
   \date   June 30, 2004

   web:   http://safetynet.devmentor.org
   email: safetynet@devmentor.org
*/

#define UTCORE_API

//#include "UTXML.h" // \todo fix xml support
#include <UnitTest.h>
#include "UnitTestAssembly.h"

// initialize class static member
UnitTestAssembly* UnitTestAssembly::sm_UnitTestAssembly = 0;
bool g_bSEHException = false;

UnitTestAssembly::UnitTestAssembly()
{
   m_Subject.clear();
#ifdef _WINDOWS
   SetErrorMode( SEM_FAILCRITICALERRORS|SEM_NOGPFAULTERRORBOX|SEM_NOOPENFILEERRORBOX );
   m_SEHFilter = SetUnhandledExceptionFilter(ryExceptionHandler);
#endif
}

// class dtor
UnitTestAssembly::~UnitTestAssembly()
{
#ifdef _WINDOWS
   SetUnhandledExceptionFilter(m_SEHFilter);
#endif
}


void UnitTestAssembly::RegisterTest(IUnitTestRunner& subject)
{
   static_cast<UnitTestRunner&>(subject).PrepareTestCases();

   // after functor list has been initialized, create a selection list
   // that will be set using the treeview ctrl to turn on/off a test case
   SubjectSelectNode subjectNode;
   subjectNode.bRunSubject = true;
   const size_t nTest = static_cast<UnitTestRunner&>( subject ).TestCaseCount();
   for( size_t i=0; i<nTest; ++i ) {
      subjectNode.TestCaseList.push_back( true );
   }
   AddSelectedSubject( subjectNode );
   m_Subject.push_back( &subject );

}

void UnitTestAssembly::Run()
{
#if 0 // \todo fix xml support
   // read UnitTest XML settings
   UTXMLDocument xmlDoc;
   LONG32 lRet = 0;
   lRet = xmlDoc.Initialize();
   lRet = xmlDoc.LoadXML( "UnitTest.xml" );
   lRet = xmlDoc.OpenNodes( "//UnitTest/*" );

   std::wstring strField;
   std::wstring strPath;
   UTXMLNodeList xmlNodeList;
   lRet = xmlDoc.GetNodeList( xmlNodeList );
   long nNodes = xmlNodeList.ChildCount();

   for( int i=0; i<nNodes; ++i )
   {
      if( strcmp( xmlNodeList.NodeNameIs( i ), "Input" ) == 0 )
      {
         UTXMLNode node;
         xmlNodeList.GetNode( i, node );

         UTXMLAttributes nodeAtrb;
         node.Attributes( nodeAtrb );

         if( strcmp( nodeAtrb.NameIs( 0 ), "field" ) == 0 )
         {
            strField = nodeAtrb.ValueIs( 0 );
            strPath  = node.NodeTextIs();
            m_mapUTInput.insert( pair_utInput( strField, strPath ) );
         }
      }
   } // for
#endif
   std::vector<IUnitTestRunner*>::iterator it = m_Subject.begin();
   m_iSubject = 0;
   while( it != m_Subject.end() )
   {
      // check if subject is selected for testing
      if( SubjectSelectIs( m_iSubject ) ) {
         IUnitTestRunner* IFace = static_cast<IUnitTestRunner*>(*it);
         IFace->RunTest();
      }
      ++m_iSubject;
      ++it;
   } // while
}

void UnitTestAssembly::RegisterObserver( IUnitTestObserver& observer )
{
   // before observer can be added, subjects must belong to the assembly!
   // since it is the job of each subject to notify each observer
   // of the unit test result
   // \todo define a assert for linux to
   //_ASSERT( ! m_Subject.empty() );

   std::vector<IUnitTestRunner*>::iterator it = m_Subject.begin();
   while( it != m_Subject.end() )
   {
      UnitTestRunner* IObj = static_cast<UnitTestRunner*>(*it);
      IObj->AddObserver( observer );
      ++it;
   }
}

int UnitTestAssembly::GetTestList( std::vector<IUnitTestRunner*>& subjectList )
{
   const int nSize = (int)m_Subject.size();
   for( int i=0; i < nSize; ++i ) {
      subjectList.push_back( m_Subject[i] );
   }
   return nSize;
}

void UnitTestAssembly::RemoveTest( IUnitTestRunner& subject )
{
   // \todo define a assert for linux to
   //_ASSERT( ! m_Subject.empty() );

   std::vector<IUnitTestRunner*>::iterator it =
   std::remove( m_Subject.begin(), m_Subject.end(), &subject );
   if( it != m_Subject.end() ) {
      m_Subject.erase( it );
   }
}

void UnitTestAssembly::ResetTestResults()
{
   const int nSize = (int)m_Subject.size();
   for( int i=0; i < nSize; ++i ) {
      static_cast<UnitTestRunner*>(m_Subject[i])->Reset();
   }
   m_iSubject   = 0; // test subject running index
   m_iTestCase  = 0; // test case running index
}

#ifdef _WINDOWS
/*---------------------------------------------------------
inline LONG WINAPI ryExceptionHandler
   (struct _EXCEPTION_POINTERS *ExceptionInfo)
 *---------------------------------------------------------*/
UTCORE_API LONG WINAPI ryExceptionHandler(struct _EXCEPTION_POINTERS *ExceptionInfo)
{
   LONG lRet = EXCEPTION_EXECUTE_HANDLER; /* EXCEPTION_CONTINUE_SEARCH */
   //LONG lRet = EXCEPTION_CONTINUE_EXECUTION; /* EXCEPTION_CONTINUE_SEARCH */
   //PEXCEPTION_RECORD per = ExceptionInfo->ExceptionRecord;
   g_bSEHException = true;
   //rySysTime();
   ////ryTrace("%s\t%s\t0x%X\t0x%X\t%d\tAn exception occured at(0x%X) ",
   //   _ryDiag.szDate,
   //   _ryDiag.szTime,
   //   _ryDiag.dwProcId,
   //   ::GetCurrentThreadId(),
   //   ryPRIORITY_CRITICAL,
   //   per->ExceptionAddress);

   //::MessageBox( 0, L"GPF Handler", L"Exception", MB_OK );
   switch(ExceptionInfo->ExceptionRecord->ExceptionCode)
   {
      /*
      The thread tried to read from or write to a virtual
      address for which it does not have the appropriate access.
      */
      case EXCEPTION_ACCESS_VIOLATION:
         /*
         The first element of the array contains a read-write flag
         that indicates the type of operation that caused the access
         violation. If this value is zero, the thread attempted to read
         the inaccessible data. If this value is 1, the thread attempted
         to write to an inaccessible address.
         */

         /*
         The second array element specifies the virtual
         address of the inaccessible data.
         */
         //ryTrace("EXCEPTION_ACCESS_VIOLATION\t");
         break;

      /*
      The thread tried to access an array element that is out of
      bounds and the underlying hardware supports bounds checking.
      */
      case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:
         //ryTrace("EXCEPTION_ARRAY_BOUNDS_EXCEEDED\t");
         break;

      /* A breakpoint was encountered. */
      case EXCEPTION_BREAKPOINT:
         //ryTrace("EXCEPTION_BREAKPOINT\t");
         break;

      /*
      The thread tried to read or write data that is misaligned
      on hardware that does not provide alignment. For example,
      16-bit values must be aligned on 2-byte boundaries; 32-bit
      values on 4-byte boundaries, and so on.
      */
      case EXCEPTION_DATATYPE_MISALIGNMENT:
         //ryTrace("EXCEPTION_DATATYPE_MISALIGNMENT\t");
         break;

      /*
      One of the operands in a floating-point operation is denormal.
      A denormal value is one that is too small to represent as a
      standard floating-point value.
      */
      case EXCEPTION_FLT_DENORMAL_OPERAND:
         //ryTrace("EXCEPTION_FLT_DENORMAL_OPERAND\t");
         break;

      /*
      The thread tried to divide a floating-point value by a
      floating-point divisor of zero.
      */
      case EXCEPTION_FLT_DIVIDE_BY_ZERO:
         //ryTrace("EXCEPTION_FLT_DIVIDE_BY_ZERO\t");
         break;

      /*
      The result of a floating-point operation cannot be represented
      exactly as a decimal fraction.
      */
      case EXCEPTION_FLT_INEXACT_RESULT:
         //ryTrace("EXCEPTION_FLT_INEXACT_RESULT\t");
         break;

      /*
      This exception represents any floating-point exception
      not included in this list.
      */
      case EXCEPTION_FLT_INVALID_OPERATION:
         //ryTrace("EXCEPTION_FLT_INVALID_OPERATION\t");
         break;

      /*
      The exponent of a floating-point operation is greater than
      the magnitude allowed by the corresponding type.
      */
      case EXCEPTION_FLT_OVERFLOW:
         //ryTrace("EXCEPTION_FLT_OVERFLOW\t");
         break;

      /*
      The stack overflowed or underflowed as the result of a
      floating-point operation.
      */
      case EXCEPTION_FLT_STACK_CHECK:
         //ryTrace("EXCEPTION_FLT_STACK_CHECK\t");
         break;

      /*
      The exponent of a floating-point operation is less
      than the magnitude allowed by the corresponding type.
      */
      case EXCEPTION_FLT_UNDERFLOW:
         //ryTrace("EXCEPTION_FLT_UNDERFLOW\t");
         break;

      /* The thread tried to execute an invalid instruction. */
      case EXCEPTION_ILLEGAL_INSTRUCTION:
         //ryTrace("EXCEPTION_ILLEGAL_INSTRUCTION\t");
         break;

      /*
      The thread tried to access a page that was not present,
      and the system was unable to load the page. For example,
      this exception might occur if a network connection is lost
      while running a program over the network.
      */
      case EXCEPTION_IN_PAGE_ERROR:
         //ryTrace("EXCEPTION_IN_PAGE_ERROR\t");
         break;

      /*
      The thread tried to divide an integer value by an
      integer divisor of zero.
      */
      case EXCEPTION_INT_DIVIDE_BY_ZERO:
         //ryTrace("Exception INTEGER DIVIDE BY ZERO\t");
         break;

      /*
      The result of an integer operation caused a carry out
      of the most significant bit of the result.
      */
      case EXCEPTION_INT_OVERFLOW:
         //ryTrace("EXCEPTION_INT_OVERFLOW\t");
         break;

      /*
      An exception handler returned an invalid disposition to
      the exception dispatcher. Programmers using a high-level
      language such as C should never encounter this exception.
      */
      case EXCEPTION_INVALID_DISPOSITION:
         //ryTrace("EXCEPTION_INVALID_DISPOSITION\t");
         break;

      /*
      The thread tried to continue execution after a
      noncontinuable exception occurred.
      */
      case EXCEPTION_NONCONTINUABLE_EXCEPTION:
         //ryTrace("EXCEPTION_NONCONTINUABLE_EXCEPTION\t");
         break;

      /*
      The thread tried to execute an instruction whose operation
      is not allowed in the current machine mode.
      */
      case EXCEPTION_PRIV_INSTRUCTION:
         //ryTrace("EXCEPTION_PRIV_INSTRUCTION\t");
         break;

      /*
      A trace trap or other single-instruction mechanism signaled
      that one instruction has been executed.
      */
      case EXCEPTION_SINGLE_STEP:
         //ryTrace("EXCEPTION_SINGLE_STEP\t");
         break;

      /* The thread used up its stack. */
      case EXCEPTION_STACK_OVERFLOW:
         //ryTrace("EXCEPTION_STACK_OVERFLOW\t");
         break;

      /* Almost everything under the sun! */
      default:
         //ryTrace("An Unhandled Exception occured\t");
         break;
   } /* switch */


#if 0
   /*
   Initialize the symbol handler
   */
   DWORD dwBaseAdrs = 0;
   HANDLE hProc = ::GetCurrentProcess();
   ::SymSetOptions(SYMOPT_CASE_INSENSITIVE |
                   SYMOPT_UNDNAME          |
                   SYMOPT_DEFERRED_LOADS   |
                   SYMOPT_LOAD_LINES);

   BOOL bRet = ::SymInitialize(hProc,
                               NULL,
                               TRUE);
   assert(TRUE == bRet);

   /*
   Enumerate and load all symbols
   */


   /*
   Load process symbol
   */
#if 0
   dwBaseAdrs = ::SymLoadModule(hProc,
                                NULL,
                                "test",
                                NULL,
                                0,
                                0);
   assert(0 != dwBaseAdrs);
#endif
   /*
   Load symbol from address
   */
   DWORD dwDisplacement = 0;
   BYTE  buffer[256];
   PIMAGEHLP_SYMBOL pSymbol = (PIMAGEHLP_SYMBOL)buffer;

   pSymbol->SizeOfStruct    = sizeof(IMAGEHLP_SYMBOL);

   pSymbol->MaxNameLength   = sizeof(buffer) -
                              sizeof(IMAGEHLP_SYMBOL) + 1;

   bRet = ::SymGetSymFromAddr(hProc,
                              (DWORD)per->ExceptionAddress,
                              &dwDisplacement,
                              pSymbol);

   assert(TRUE == bRet);

   /*
   Get filename and line number
   */
   IMAGEHLP_LINE image_line;
   image_line.SizeOfStruct = sizeof(IMAGEHLP_LINE);
   image_line.Address      = (DWORD)per->ExceptionAddress;

   bRet = ::SymGetLineFromAddr(hProc,
                               (DWORD)per->ExceptionAddress,
                               &dwDisplacement,
                               &image_line);

   assert(TRUE == bRet);

   //ryTrace("%s(%ld)\n", image_line.FileName,
                        image_line.LineNumber);

   /*
   Get undecorated name
   */
#if 0
   TCHAR tszName[_MAX_PATH];
   TCHAR tszSymSearchPath[_MAX_PATH];

   bRet = ::SymUnDName(pSymbol,
                       tszName,
                       sizeof(tszName));

   assert(TRUE == bRet);

   ::SymGetSearchPath(GetCurrentProcess(),
                      tszSymSearchPath,
                      _MAX_PATH);


   PIMAGE_DEBUG_INFORMATION pdbgImageInfo = NULL;
   pdbgImageInfo = ::MapDebugInformation(NULL,
                                         __FILE__,
                                         tszSymSearchPath,
                                         0);

   assert(NULL != pdbgImageInfo);

   //ryTrace("\t\t\t\tsymbol name is %s (%s)\n",
      pdbgImageInfo->ImageFileName, tszName);
   //ryTrace("\t\t\t\tsymbol path is %s \n\n",
      pdbgImageInfo->ImageFilePath);



   /*
   Release symbol info
   */
   if(pdbgImageInfo) {
      UnmapDebugInformation(pdbgImageInfo);
   }
   pdbgImageInfo = NULL;

#endif


   /*
   Clean up
   */
   ::SymUnloadModule(hProc, dwBaseAdrs);
   ::SymCleanup(hProc);

   fflush(_ryDiag.fdMsgLog);
#endif

   return lRet;
}
#endif // _WINDOWS
