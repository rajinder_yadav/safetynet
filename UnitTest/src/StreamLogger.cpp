/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

/** \brief StreamLogger logs unit test results to a file

   \file StreamLogger.cpp
   
   \author Rajinder Yadav
   \date   July 4, 2004

   web:   http://safetynet.devmentor.org
   email: safetynet@devmentor.org
*/

#define UTCORE_API

#include <UnitTest.h>
#include "StreamLogger.h"
#include <fstream>
#include <iostream>
#include <ctime>
#include <sstream>

using std::endl;

#pragma warning ( disable : 4996 )

StreamLogger::StreamLogger() :
   m_nTestRun( 0 ),
   m_nFailed( 0 ),
   m_nExceptions( 0 )
{
}

StreamLogger::StreamLogger( std::string strFilename ) :
   m_strFilename( strFilename ),
   m_nTestRun( 0 ),
   m_nFailed( 0 ),
   m_nExceptions( 0 )
{
   Open( strFilename );
}

StreamLogger::~StreamLogger()
{
   if( m_LogFile.is_open() ) {
      Close();
   }
}

void StreamLogger::Open( std::string strFilename )
{
   m_nTestRun    = 0;
   m_nFailed     = 0;
   m_nExceptions = 0;
   m_strFilename = strFilename;
   m_LogFile.open( m_strFilename.c_str() );
}

void StreamLogger::Close()
{
   if( m_ssErrors.rdbuf()->in_avail() > 0 )
   {
      m_LogFile
      << "-------------------" << endl
      << "Summary of Failures" << endl
      << "-------------------" << endl
      << m_ssErrors.str()      << endl;
   }

   m_LogFile
   << "----------------"    << endl
   << "Overall Summary"     << endl
   << "----------------"    << endl
   << "Total Run: "         << m_nTestRun    << endl
   << "Total Failed: "      << m_nFailed     << endl
   << "Total Exceptions: "  << m_nExceptions << endl;

   if ( m_LogFile.is_open() )
   {
      m_LogFile.flush();
      m_LogFile.close();
   }
}

void StreamLogger::UnitTestStart(IUnitTestRunner& subject)
{
   // format date
   time_t tm = {0};
   time( &tm );
   struct tm* pstm =localtime( &tm );

   char wzDate[40] = {0};
   strftime( wzDate, 40, "%B %d, %Y %H:%M:%S", pstm );

   m_LogFile
   << "-----------------------------------------" << endl
   << "Date: " << wzDate << endl
   << "Test Runner: " << static_cast<UnitTestRunner&>(subject).ClassNameIs() << endl
   << "-----------------------------------------" << endl
   << endl
   << "Unit Test Started" << endl;
}

void StreamLogger::Failed(IUnitTestRunner& subject)
{
   m_LogFile 
   << static_cast<UnitTestRunner&>(subject).TestMsgIs()
   << endl;

   m_ssErrors
   << static_cast<UnitTestRunner&>(subject).TestMsgIs()
   << endl;
}

void StreamLogger::Passed(IUnitTestRunner& subject)
{
   m_LogFile 
   << static_cast<UnitTestRunner&>(subject).TestMsgIs()
   << endl;
}

void StreamLogger::Exception(IUnitTestRunner& subject)
{
   m_LogFile 
   << static_cast<UnitTestRunner&>(subject).TestMsgIs()
   << endl;

   m_ssErrors
   << static_cast<UnitTestRunner&>(subject).TestMsgIs()
   << endl;
}

void StreamLogger::UnitTestEnd(IUnitTestRunner& subject)
{
   m_LogFile
   << "Unit Test complete. "
   << endl << endl
   << "Test Summary: Tests("
   << static_cast<UnitTestRunner&>(subject).TestCountIs() << ")"
   << " Fails("
   << static_cast<UnitTestRunner&>(subject).FailedCountIs() << ")"
   << " Exceptions("
   << static_cast<UnitTestRunner&>(subject).ExceptionCountIs() << ")"
   << endl << endl;

   m_nTestRun    += static_cast<UnitTestRunner&>(subject).TestCountIs();
   m_nFailed     += static_cast<UnitTestRunner&>(subject).FailedCountIs();
   m_nExceptions += static_cast<UnitTestRunner&>(subject).ExceptionCountIs();
}
