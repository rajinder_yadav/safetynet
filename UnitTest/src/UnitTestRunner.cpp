/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

/** \brief UnitTestRunner is the main test engine

   \file UnitTestRunner.cpp

   \author Rajinder Yadav
   \date   June 30, 2004

   web:   http://safetynet.devmentor.org
   email: safetynet@devmentor.org
*/

#define UTCORE_API

#include <UnitTest.h>
#include "UnitTestRunner.h"

UnitTestRunner::UnitTestRunner(const char* const name) :
   m_strTestClass(name),
   m_nTestCases(0),
   m_nFailed(0),
   m_nExceptions(0),
   m_bPrepareTestCases( false )
{
   m_strMessageStream.rdbuf()->str( "" );
}

UnitTestRunner::~UnitTestRunner()
{
}

void UnitTestRunner::ExecuteTest()
{
   NofityObserver( TC_EVENT_START );
   const size_t nTest = TestCaseCount();
   for( int i=0; i < (int)nTest; ++i )
   {
      // only execute those test cases that have been selected to run
      if( ! UnitTestAssembly::GetInstance().SubjectTestCaseSelectIs(i) ) {
         continue;
      }

      try {
            Setup();
            IncTestCount();
            TestCaseIs( m_strTestName[i].c_str() );
            if( ExecuteTestCase(i) )
            {
               ClearTestMsg();
               this->m_strMessageStream
                  << "[PASS] Test case: " << ClassNameIs()
                  << "::" << TestCaseIs() << "()";
               NofityObserver( TC_EVENT_PASSED );
            }
            else
            {
               IncFailed();
               ClearTestMsg();
               this->m_strMessageStream
               << "[FAIL] Test case: " << ClassNameIs()
               << "::" << TestCaseIs()
               << "()";
               NofityObserver( TC_EVENT_FAILED );
            }
            CleanUp();
      }
      catch( ... ) {
               IncException();
               ClearTestMsg();
               this->m_strMessageStream
               << "[EXCEPTION] Test case: " << ClassNameIs()
               << "::" << TestCaseIs()
               << "() unhandled C++ exception";
               NofityObserver( TC_EVENT_EXCEPTION );
      }
   } // for
   NofityObserver( TC_EVENT_END );
}


void UnitTestRunner::NofityObserver(TESTCASE_EVENT eResult)
{
   std::vector<IUnitTestObserver*>::iterator it = m_ObserverList.begin();

   while( it != m_ObserverList.end() )
   {
      IUnitTestObserver* observer = static_cast<IUnitTestObserver*>(*it);

      // optimized in most likely, most occuring order
      // maintain case order as declared in the enum TESTCASE_EVENT
      switch ( eResult )
      {
      case TC_EVENT_PASSED:
         observer->Passed( static_cast<IUnitTestRunner&>(*this) );
         break;

      case TC_EVENT_START:
         observer->UnitTestStart( static_cast<IUnitTestRunner&>(*this) );
         break;

      case TC_EVENT_END:
         observer->UnitTestEnd( static_cast<IUnitTestRunner&>(*this) );
         break;

      case TC_EVENT_FAILED:
         observer->Failed( static_cast<IUnitTestRunner&>(*this) );
         break;

      case TC_EVENT_EXCEPTION:
         observer->Exception( static_cast<IUnitTestRunner&>(*this) );
         break;

      default:
         // \todo define a assert for linux to
         //_ASSERT(false);  // unknown event
         break;
      } // switch
      ++it;
   } // while
}
