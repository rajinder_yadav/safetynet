/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

/** \brief Log unit test result to the console window

   \file ConsoleLogger.cpp

   \author Rajinder Yadav
   \date   July 5, 2004

   web:   http://safetynet.devmentor.org
   email: safetynet@devmentor.org
*/

#define UTCORE_API

#include <UnitTest.h>
#include "ConsoleLogger.h"
#include <iostream>
#include <ctime>
#include <sstream>

using std::cout;
using std::endl;

#pragma warning ( disable : 4996 )

ConsoleLogger::ConsoleLogger()
{
   m_nTestRun    = 0;
   m_nFailed     = 0;
   m_nExceptions = 0;
}

ConsoleLogger::~ConsoleLogger()
{
   if( m_ssErrors.rdbuf()->in_avail() > 0 )
   {
      cout << "-------------------" << endl
           << "Summary of Failures" << endl
           << "-------------------" << endl
           << m_ssErrors.str()      << endl;
   }
   
   cout  << "----------------"   << endl
         << "Overall Summary"    << endl
         << "----------------"   << endl
         << "Total Run: "        << m_nTestRun << endl
         << "Total Failed: "     << m_nFailed << endl
         << "Total Exceptions: " << m_nExceptions << endl;
}

void ConsoleLogger::UnitTestStart(IUnitTestRunner& subject)
{
   // format date
   time_t tm = {0};
   time( &tm );
   struct tm* pstm =localtime( &tm );

   char wzDate[40] = {0};
   strftime( wzDate, 40, "%B %d, %Y %H:%M:%S", pstm );

   cout << "-----------------------------------------" << endl
        << "Date: " << wzDate << endl
        << "Test Runner: " << static_cast<UnitTestRunner&>(subject).ClassNameIs() << endl
        << "-----------------------------------------" << endl
        << endl
        << "Unit Test Started" << endl;
}

void ConsoleLogger::Failed(IUnitTestRunner& subject)
{
   cout << static_cast<UnitTestRunner&>(subject).TestMsgIs() << endl;
  m_ssErrors << static_cast<UnitTestRunner&>(subject).TestMsgIs() << endl;
}

void ConsoleLogger::Passed(IUnitTestRunner& subject)
{
   cout << static_cast<UnitTestRunner&>(subject).TestMsgIs() << endl;
}

void ConsoleLogger::Exception(IUnitTestRunner& subject)
{
   cout << static_cast<UnitTestRunner&>(subject).TestMsgIs() << endl;
   m_ssErrors << static_cast<UnitTestRunner&>(subject).TestMsgIs() << endl;
}

void ConsoleLogger::UnitTestEnd(IUnitTestRunner& subject)
{
   cout << "Unit Test complete. "
        << endl << endl
        << "Test Summary: Tests("
        << static_cast<UnitTestRunner&>(subject).TestCountIs() << ")"
        << " Fails("
        << static_cast<UnitTestRunner&>(subject).FailedCountIs() << ")"
        << " Exceptions("
        << static_cast<UnitTestRunner&>(subject).ExceptionCountIs() << ")"
        << endl << endl;

   m_nTestRun    += static_cast<UnitTestRunner&>(subject).TestCountIs();
   m_nFailed     += static_cast<UnitTestRunner&>(subject).FailedCountIs();
   m_nExceptions += static_cast<UnitTestRunner&>(subject).ExceptionCountIs();
}
