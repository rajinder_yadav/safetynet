/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

#include "person.h"
#include <string>

Person::Person( const int age, const std::string& name )
: _age( age )
, _name( name )
{
   
}

Person::Person( const Person& obj )
{
   _age  = obj._age;
   _name = obj._name;
}

Person& Person::operator=( const Person& obj )
{
   // avoid self-assignment
   if( &obj == this) return *this;
   _age  = obj._age;
   _name = obj._name;
   return *this;
}

void Person::setAge( const int age )
{
   _age = age;   
}

const int Person::getAge() const
{
   return _age;
}

void Person::setName( const std::string& name )
{
   _name = name;
}

std::string Person::getName() const
{
   return _name;   
}
