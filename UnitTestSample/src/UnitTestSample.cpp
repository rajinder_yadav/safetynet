/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

// Source: SpotLightTest.cpp
// Author: Rajinder Yadav
// Date:   June 30, 2004
//
// web:   http://safetynet.devmentor.org
// email: safetynet@devmentor.org


#include <UnitTest.h>
#include "MyTestFoo.h"
#include "MyTestBar.h"

int main(int /*argc*/, char** /*argv[]*/)
{

   // setup console logging
   ConsoleLogger display;

   // setup test result observer
   DECLARE_OBSERVER( display );

   // begin unit testing
   // NOTE: GUI Test Monitor and file logger observer are
   //       created when we start the unit test with this macro
   START_UNIT_TEST( "UnitTest.log" );
	return 0;
}
