/*================================================================================
 Apache License Version 2.0
 
 Copyright 2015 Rajinder Yadav <safetynet@devmentor.org>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ================================================================================*/

// Source: MyTestFoo.h
// Source: 
// Author: Rajinder Yadav
// Date:   June 30, 2004
//
// web:   http://safetynet.devmentor.org
// email: safetynet@devmentor.org


class MyTestFoo : public UnitTestRunner
{
   bool TestFoo1();
   bool TestFoo2();
   bool TestFoo3();

public:

   REGISTER_TESTCLASS( MyTestFoo )

   void RunTest()
   {
      UNIT_TEST_START();

         UNIT_TEST(TestFoo1);
         UNIT_TEST(TestFoo2);
         UNIT_TEST(TestFoo3);

      UNIT_TEST_END();
   }

};
